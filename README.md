# socialnet

**Functionality:**  

+ registration  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ creating friendship relationship  
+ group registration  
+ upload and download avatar  
+ users export to xml  
+ users import from xml  

**Tools:**  
JDK 8, Spring 4, JPA 2 / Hibernate 4, XStream, jQuery 3, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 2017.1.  

**Demo:**  
https://socialnet-demo.herokuapp.com  
Login: test@test.com  
Password: 12345  

**Screenshots:**  
https://drive.google.com/open?id=0B2yUGkRdju8eY2FyNVJJZXRQaHM  
https://drive.google.com/open?id=0B2yUGkRdju8eX2UwaTVpRE90bE0  
https://drive.google.com/open?id=0B2yUGkRdju8eLU16YVZRN0NVakE  
https://drive.google.com/open?id=0B2yUGkRdju8eQUdhRTZLZVJTVEU  
https://drive.google.com/open?id=0B2yUGkRdju8eRzRwVk5PbDEwcFU  

--  
**Vasilenko Evgeniy**  
Training getJavaJob,  
[http://www.getjavajob.com](http://www.getjavajob.com)
