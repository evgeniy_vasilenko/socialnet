package com.getjavajob.training.webproject1702.vasilenkoe.common;

import javax.persistence.*;

/**
 * Created by Evgeniy on 26.04.2017.
 */
@javax.persistence.Entity
@Table(name = "friends")
public class AccountFriend implements Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_1")
    private Account acc1;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_2")
    private Account acc2;

    @ManyToOne
    @JoinColumn(name = "REQUEST_RECEIVER")
    private Account requestReceiver;

    @Column(name = "ACCEPTED")
    private boolean accepted;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAcc1() {
        return acc1;
    }

    public void setAcc1(Account acc1) {
        this.acc1 = acc1;
    }

    public Account getAcc2() {
        return acc2;
    }

    public void setAcc2(Account acc2) {
        this.acc2 = acc2;
    }

    public Account getRequestReceiver() {
        return requestReceiver;
    }

    public void setRequestReceiver(Account requestReceiver) {
        this.requestReceiver = requestReceiver;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountFriend)) return false;

        AccountFriend that = (AccountFriend) o;

        if (!acc1.equals(that.acc1)) return false;
        return acc2.equals(that.acc2);
    }

    @Override
    public int hashCode() {
        int result = acc1.hashCode();
        result = 31 * result + acc2.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AccountFriend{" +
                "id=" + id +
                ", acc1=" + acc1 +
                ", acc2=" + acc2 +
                ", requestReceiver=" + requestReceiver +
                ", accepted=" + accepted +
                '}';
    }
}
