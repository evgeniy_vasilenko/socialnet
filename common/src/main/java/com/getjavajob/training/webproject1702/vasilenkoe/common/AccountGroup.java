package com.getjavajob.training.webproject1702.vasilenkoe.common;

import javax.persistence.*;

/**
 * Created by Evgeniy on 26.04.2017.
 */
@javax.persistence.Entity
@Table(name = "account_group")
public class AccountGroup implements Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "GROUP_ID")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    private GroupRole role;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public GroupRole getRole() {
        return role;
    }

    public void setRole(GroupRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountGroup)) return false;

        AccountGroup that = (AccountGroup) o;

        if (account != null ? !account.equals(that.account) : that.account != null) return false;
        if (group != null ? !group.equals(that.group) : that.group != null) return false;
        return role != null ? role.equals(that.role) : that.role == null;
    }

    @Override
    public int hashCode() {
        int result = account != null ? account.hashCode() : 0;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AccountGroup{" +
                "id=" + id +
                ", account=" + account +
                ", group=" + group +
                ", role=" + role +
                '}';
    }
}
