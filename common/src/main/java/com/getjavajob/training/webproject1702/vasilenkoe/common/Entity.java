package com.getjavajob.training.webproject1702.vasilenkoe.common;

/**
 * Created by Evgeniy on 08.03.2017.
 */
public interface Entity {
    Integer getId();

    void setId(Integer id);
}
