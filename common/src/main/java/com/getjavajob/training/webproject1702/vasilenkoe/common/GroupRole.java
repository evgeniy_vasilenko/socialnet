package com.getjavajob.training.webproject1702.vasilenkoe.common;

import javax.persistence.*;

/**
 * Created by Evgeniy on 11.06.2017.
 */
@javax.persistence.Entity
@Table(name = "group_role")
public class GroupRole implements Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String role) {
        this.name = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupRole)) return false;

        GroupRole groupRole = (GroupRole) o;

        return name.equals(groupRole.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
