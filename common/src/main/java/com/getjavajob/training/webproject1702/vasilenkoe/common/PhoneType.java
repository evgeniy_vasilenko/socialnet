package com.getjavajob.training.webproject1702.vasilenkoe.common;

import javax.persistence.*;

/**
 * Created by Evgeniy on 11.06.2017.
 */
@javax.persistence.Entity
@Table(name = "phone_type")
public class PhoneType implements Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "TYPE")
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }
}
