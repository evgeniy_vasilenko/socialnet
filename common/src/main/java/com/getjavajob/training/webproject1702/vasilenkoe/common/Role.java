package com.getjavajob.training.webproject1702.vasilenkoe.common;

import javax.persistence.*;

/**
 * Created by Evjen on 15.08.2017.
 */
@javax.persistence.Entity
@Table(name = "roles")
public class Role implements Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String role) {
        this.name = role;
    }
}
