package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountFriend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Evjen on 08.08.2017.
 */
public interface AccountFriendRepository extends JpaRepository<AccountFriend, Integer> {

    @Query("select accFrnd from AccountFriend accFrnd where accFrnd.acc1.id = :id or accFrnd.acc2.id = :id")
    List<AccountFriend> getAccountFriends(@Param("id") int id);

    @Query("select accFrnd from AccountFriend accFrnd where accFrnd.acc1 = :#{#accountFriend.acc1} and accFrnd.acc2 = :#{#accountFriend.acc2}")
    AccountFriend getAccountFriend(@Param("accountFriend") AccountFriend accountFriend);

    long countAccountFriendByRequestReceiverEqualsAndAcceptedFalse(Account account);
}
