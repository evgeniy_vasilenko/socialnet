package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Evjen on 07.08.2017.
 */
public interface AccountGroupRepository extends JpaRepository<AccountGroup, Integer> {

    @Modifying
    @Query("delete from AccountGroup where account.id = :accId and group.id = :groupId")
    void remove(@Param("accId") int accId, @Param("groupId") int groupId);
}
