package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Evjen on 07.08.2017.
 */
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Modifying
    @Query("update Account set password = :#{#account.password} where id = :#{#account.id}")
    void updatePassword(@Param("account") Account account);

    @Modifying
    @Query("update Account set role = :#{#account.role} where id = :#{#account.id}")
    void updateRole(@Param("account") Account account);

    Account getByEmail(String email);

    List<Account> findByLastNameContainsOrFirstNameContains(String lName, String fName);

    List<Account> findByLastNameContainsOrFirstNameContains(String lName, String fName, Pageable pageable);

    long countAccountByLastNameContainsOrFirstNameContains(String lName, String fName);

    @Modifying
    @Query("update Account set image = :image where id = :id")
    void updateImage(@Param("id") int objectId, @Param("image") Image image);

    @Modifying
    @Query("delete from Phone where account = :account")
    void deletePhonesForAccount(@Param("account") Account account);

}
