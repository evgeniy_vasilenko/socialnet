package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Evjen on 08.08.2017.
 */
public interface GroupRepository extends JpaRepository<Group, Integer> {

    List<Group> getByName(String name);

    @Modifying
    @Query("update Group set image = :image where id = :id")
    void updateImage(@Param("id") int objectId, @Param("image") Image image);

    List<Group> findByNameContainsOrDirectionContains(String name, String direction, Pageable pageable);

    long countGroupByNameContainsOrDirectionContains(String name, String direction);

}
