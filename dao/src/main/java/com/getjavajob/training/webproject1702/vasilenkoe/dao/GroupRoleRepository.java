package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.GroupRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Evjen on 05.09.2017.
 */
public interface GroupRoleRepository extends JpaRepository<GroupRole, Integer> {

    GroupRole getByName(String name);

    @Query("select accGroup.role from AccountGroup accGroup where accGroup.account.id = :accId and accGroup.group.id = :groupId")
    GroupRole getGroupRole(@Param("accId") int accId, @Param("groupId") int groupId);

}
