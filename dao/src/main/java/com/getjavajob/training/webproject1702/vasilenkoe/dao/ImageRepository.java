package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Evjen on 07.08.2017.
 */
public interface ImageRepository extends JpaRepository<Image, Integer> {
}
