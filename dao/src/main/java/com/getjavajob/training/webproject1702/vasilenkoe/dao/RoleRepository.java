package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Evjen on 17.08.2017.
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role getByName(String name);

}
