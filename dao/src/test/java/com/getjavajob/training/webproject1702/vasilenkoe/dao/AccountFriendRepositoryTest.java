package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountFriend;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Evgeniy on 26.04.2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class AccountFriendRepositoryTest {

    @Autowired
    private AccountFriendRepository accountFriendRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void getTest() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        Account account2 = new Account();
        account2.setEmail("test2@test");

        AccountFriend accountFriend = new AccountFriend();
        accountFriend.setAcc1(account1);
        accountFriend.setAcc2(account2);

        Integer id = entityManager.persistAndGetId(accountFriend, Integer.class);

        assertEquals(accountFriend, accountFriendRepository.findOne(id));
    }

    @Test
    public void insertTest() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        Account account2 = new Account();
        account2.setEmail("test2@test");

        entityManager.persist(account1);
        entityManager.persist(account2);

        AccountFriend accountFriend = new AccountFriend();
        accountFriend.setAcc1(account1);
        accountFriend.setAcc2(account2);

        List<AccountFriend> accountFriends = new ArrayList<>();

        assertEquals(accountFriends, accountFriendRepository.findAll());
        accountFriends.add(accountFriend);
        accountFriendRepository.save(accountFriend);
        assertEquals(accountFriends, accountFriendRepository.findAll());
    }

    @Test
    public void updateTest() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        Account account2 = new Account();
        account2.setEmail("test2@test");

        AccountFriend accountFriend = new AccountFriend();
        accountFriend.setAcc1(account1);
        accountFriend.setAcc2(account2);

        Integer id = entityManager.persistAndGetId(accountFriend, Integer.class);
        entityManager.detach(accountFriend);
        accountFriend.setId(id);

        assertFalse(accountFriendRepository.findOne(id).isAccepted());
        accountFriend.setAccepted(true);
        accountFriendRepository.save(accountFriend);
        assertTrue(accountFriendRepository.findOne(id).isAccepted());
    }

    @Test
    public void deleteTest() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        Account account2 = new Account();
        account2.setEmail("test2@test");

        AccountFriend accountFriend = new AccountFriend();
        accountFriend.setAcc1(account1);
        accountFriend.setAcc2(account2);

        Integer id = entityManager.persistAndGetId(accountFriend, Integer.class);

        accountFriendRepository.delete(id);

        assertNull(accountFriendRepository.findOne(id));
    }

    @Test
    public void getAccountFriendsTest() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        Account account2 = new Account();
        account2.setEmail("test2@test");

        Integer id = entityManager.persistAndGetId(account1, Integer.class);
        entityManager.persist(account2);

        AccountFriend accountFriend = new AccountFriend();
        accountFriend.setAcc1(account1);
        accountFriend.setAcc2(account2);

        List<AccountFriend> accountFriends = new ArrayList<>();
        accountFriends.add(accountFriend);

        accountFriendRepository.save(accountFriend);

        assertEquals(accountFriends, accountFriendRepository.getAccountFriends(id));
    }
}
