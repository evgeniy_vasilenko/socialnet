package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountGroup;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.common.GroupRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Evgeniy on 26.04.2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class AccountGroupRepositoryTest {

    @Autowired
    private AccountGroupRepository accountGroupRepository;

    @Autowired
    private GroupRoleRepository groupRoleRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findOneTest() {
        Account account = new Account();
        account.setEmail("test@test");
        entityManager.persist(account);

        Group group = new Group();
        group.setName("test");
        entityManager.persist(group);

        AccountGroup accountGroup = new AccountGroup();
        accountGroup.setAccount(account);
        accountGroup.setGroup(group);

        Integer id = entityManager.persistAndGetId(accountGroup, Integer.class);

        assertEquals(accountGroup, accountGroupRepository.findOne(id));
    }

    @Test
    public void insertTest() {
        Account account = new Account();
        account.setEmail("test@test");
        entityManager.persist(account);

        Group group = new Group();
        group.setName("test");
        entityManager.persist(group);

        AccountGroup accountGroup = new AccountGroup();
        accountGroup.setAccount(account);
        accountGroup.setGroup(group);

        List<AccountGroup> accountGroups = new ArrayList<>();
        assertEquals(accountGroups, accountGroupRepository.findAll());
        accountGroups.add(accountGroup);
        accountGroupRepository.save(accountGroup);
        assertEquals(accountGroups, accountGroupRepository.findAll());
    }

    @Test
    public void updateTest() {
        Account account = new Account();
        account.setEmail("test@test");
        entityManager.persist(account);

        Group group = new Group();
        group.setName("test");
        entityManager.persist(group);

        AccountGroup accountGroup = new AccountGroup();
        accountGroup.setAccount(account);
        accountGroup.setGroup(group);

        Integer id = entityManager.persistAndGetId(accountGroup, Integer.class);
        entityManager.detach(accountGroup);

        GroupRole groupRole = new GroupRole();
        groupRole.setName("MEMBER");
        entityManager.persist(groupRole);

        accountGroup.setRole(groupRole);

        accountGroupRepository.save(accountGroup);

        assertEquals("MEMBER", accountGroupRepository.findOne(id).getRole().getName());
    }

    @Test
    public void deleteTest() {
        accountGroupRepository.remove(1, 1);
        assertNull(accountGroupRepository.findOne(1));

        Account account = new Account();
        account.setEmail("test@test");
        entityManager.persist(account);

        Group group = new Group();
        group.setName("test");
        entityManager.persist(group);

        AccountGroup accountGroup = new AccountGroup();
        accountGroup.setAccount(account);
        accountGroup.setGroup(group);

        Integer id = entityManager.persistAndGetId(accountGroup, Integer.class);

        accountGroupRepository.delete(id);

        assertNull(accountGroupRepository.findOne(id));
    }

    @Test
    public void accountGroupRoleTest() {
        Account account = new Account();
        account.setEmail("test@test");
        int accId = entityManager.persistAndGetId(account, Integer.class);

        Group group = new Group();
        group.setName("test");
        int groupId = entityManager.persistAndGetId(group, Integer.class);

        GroupRole groupRole = new GroupRole();
        groupRole.setName("ADMIN");
        entityManager.persist(groupRole);

        AccountGroup accountGroup = new AccountGroup();
        accountGroup.setAccount(account);
        accountGroup.setGroup(group);
        accountGroup.setRole(groupRole);
        entityManager.persist(accountGroup);

        assertEquals("ADMIN", groupRoleRepository.getGroupRole(accId, groupId).getName());
    }

}
