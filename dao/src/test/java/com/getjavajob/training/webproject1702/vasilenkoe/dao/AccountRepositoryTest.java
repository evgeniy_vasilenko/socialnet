package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Evjen on 02.09.2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findOneTest() {
        Account account = new Account();
        account.setEmail("test@test.com");
        Integer id = entityManager.persistAndGetId(account, Integer.class);

        assertEquals(account, accountRepository.findOne(id));
    }

    @Test
    public void findOneNullTest() {
        assertNull(accountRepository.findOne(4));
    }

    @Test
    public void findAllTest() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        Account account2 = new Account();
        account2.setEmail("test2@test");

        entityManager.persist(account1);
        entityManager.persist(account2);

        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(account1);
        expectedAccounts.add(account2);

        assertEquals(expectedAccounts, accountRepository.findAll());
    }

    @Test
    public void insertTest() {
        Account account = new Account();
        account.setEmail("test@test.com");

        assertNull(accountRepository.getByEmail("test@test.com"));
        accountRepository.save(account);
        assertEquals(account, accountRepository.getByEmail("test@test.com"));
    }

    @Test
    public void updateTest() {
        Account account = new Account();
        account.setEmail("test@test");

        Integer id = entityManager.persistAndGetId(account, Integer.class);

        Account updAccount = new Account();
        updAccount.setId(id);
        updAccount.setEmail("updated@test");
        accountRepository.saveAndFlush(updAccount);

        assertEquals(updAccount, accountRepository.findOne(id));
    }

    @Test
    public void deleteTest() {
        Account account = new Account();
        account.setEmail("test@test");

        Integer id = entityManager.persistAndGetId(account, Integer.class);

        assertEquals(account, accountRepository.findOne(id));
        accountRepository.delete(id);
        assertNull(accountRepository.findOne(id));
    }

    @Test
    public void getByNameTest1() {
        Account account = new Account();
        account.setEmail("test@test");
        account.setFirstName("ivan");

        entityManager.persist(account);

        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(account);

        assertEquals(expectedAccounts, accountRepository.findByLastNameContainsOrFirstNameContains("ivan", "ivan"));
    }

    @Test
    public void getByNameTest2() {
        Account account = new Account();
        account.setEmail("test@test");
        account.setLastName("ivan");

        entityManager.persist(account);

        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(account);

        assertEquals(expectedAccounts, accountRepository.findByLastNameContainsOrFirstNameContains("ivan", "ivan"));
    }

    @Test
    public void getByNameTest3() {
        Account account = new Account();
        account.setEmail("test@test");
        account.setLastName("ivan");

        entityManager.persist(account);

        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(account);

        assertEquals(expectedAccounts, accountRepository.findByLastNameContainsOrFirstNameContains("an", "an"));
    }

    @Test
    public void getByNameTest4() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        account1.setLastName("Ivanov");
        Account account2 = new Account();
        account2.setEmail("test2@test");
        account2.setLastName("Petrov");

        entityManager.persist(account1);
        entityManager.persist(account2);

        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(account1);
        expectedAccounts.add(account2);
        Pageable pageable = new PageRequest(0, 2);

        assertEquals(expectedAccounts, accountRepository.findByLastNameContainsOrFirstNameContains("ov", "ov", pageable));
    }

    @Test
    public void getByNameCountTest() {
        Account account1 = new Account();
        account1.setEmail("test1@test");
        account1.setLastName("Ivanov");
        Account account2 = new Account();
        account2.setEmail("test2@test");
        account2.setLastName("Petrov");

        entityManager.persist(account1);
        entityManager.persist(account2);

        assertEquals(2, accountRepository.countAccountByLastNameContainsOrFirstNameContains("ov", "ov"));
    }

    @Test
    public void updatePasswordTest() {
        Account account = new Account();
        account.setEmail("test@test");
        account.setPassword("123");

        Integer id = entityManager.persistAndGetId(account, Integer.class);
        entityManager.detach(account);

        Account updAccount = new Account();
        updAccount.setId(id);
        updAccount.setEmail("test@test");
        updAccount.setPassword("321");

        accountRepository.updatePassword(updAccount);

        assertEquals("321", accountRepository.findOne(id).getPassword());
    }

    @Test
    public void updateImage() {
        Image image = new Image();
        image.setName("image");

        Integer imgId = entityManager.persistAndGetId(image, Integer.class);
        entityManager.detach(image);

        Account account = new Account();
        account.setEmail("test@test");

        Integer accId = entityManager.persistAndGetId(account, Integer.class);
        entityManager.detach(account);

        accountRepository.updateImage(accId, entityManager.find(Image.class, imgId));

        assertEquals(image, accountRepository.findOne(accId).getImage());
    }

    @Test
    public void updateRoleTest() {
        Role role = new Role();
        role.setName("ROLE_ADMIN");
        Integer id = entityManager.persistAndGetId(role, Integer.class);

        Account account = new Account();
        account.setEmail("test@test");
        accountRepository.save(account);

        account.setRole(entityManager.find(Role.class, id));
        accountRepository.updateRole(account);

        assertEquals("ROLE_ADMIN", accountRepository.getByEmail("test@test").getRole().getName());
    }

    @Test
    public void getByEmail() {
        Account account = new Account();
        account.setEmail("test@test");
        entityManager.persist(account);

        assertEquals(account, accountRepository.getByEmail("test@test"));
    }
}
