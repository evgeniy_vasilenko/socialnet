package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Evgeniy on 28.02.2017.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class GroupRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findOneTest() {
        Group group = new Group();
        group.setName("test");

        Integer id = entityManager.persistAndGetId(group, Integer.class);

        assertEquals(group, groupRepository.findOne(id));
    }

    @Test
    public void findOneNullTest() {
        assertNull(groupRepository.findOne(1));
    }

    @Test
    public void findAllTest() {
        Group group1 = new Group();
        group1.setName("test1");
        Group group2 = new Group();
        group2.setName("test2");

        entityManager.persist(group1);
        entityManager.persist(group2);

        List<Group> groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);

        assertEquals(groups, groupRepository.findAll());
    }

    @Test
    public void insertTest() {
        Group group = new Group();
        group.setName("test");

        List<Group> groups = new ArrayList<>();

        assertEquals(groups, groupRepository.findAll());
        groups.add(group);
        groupRepository.save(group);
        assertEquals(groups, groupRepository.findAll());
    }

    @Test
    public void updateTest() {
        Group group = new Group();
        group.setName("test");

        Integer id = entityManager.persistAndGetId(group, Integer.class);
        entityManager.detach(group);

        group.setName("test2");
        groupRepository.save(group);

        assertEquals(group, groupRepository.findOne(id));
    }

    @Test
    public void deleteTest() {
        Group group = new Group();
        group.setName("test");

        Integer id = entityManager.persistAndGetId(group, Integer.class);
        entityManager.detach(group);

        groupRepository.delete(id);
        assertNull(groupRepository.findOne(id));
    }

    @Test
    public void getByGroupNameTest() {
        Group group1 = new Group();
        group1.setName("test1");
        Group group2 = new Group();
        group2.setName("test2");

        entityManager.persist(group1);
        entityManager.persist(group2);

        List<Group> groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);

        assertEquals(groups, groupRepository.findByNameContainsOrDirectionContains("te", "te", new PageRequest(0, 2)));
    }

    @Test
    public void updateImage() {
        Image image = new Image();
        image.setName("image");

        Integer imgId = entityManager.persistAndGetId(image, Integer.class);
        entityManager.detach(image);

        Account account = new Account();
        account.setEmail("test@test");

        Group group = new Group();
        group.setName("test");

        Integer groupId = entityManager.persistAndGetId(group, Integer.class);
        entityManager.detach(group);

        groupRepository.updateImage(groupId, entityManager.find(Image.class, imgId));

        assertEquals(image, groupRepository.findOne(groupId).getImage());
    }
}
