package com.getjavajob.training.webproject1702.vasilenkoe.dao;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Evgeniy on 01.05.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@Transactional
public class ImageRepositoryTest {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findOneTest() {
        Image image = new Image();
        Integer id = entityManager.persistAndGetId(image, Integer.class);

        assertNotNull(imageRepository.findOne(id));
    }

    @Test
    public void insertTest() {
        Image image = new Image();
        image.setName("test");

        List<Image> images = new ArrayList<>();

        assertEquals(images, imageRepository.findAll());
        imageRepository.save(image);
        images.add(image);
        assertEquals(images, imageRepository.findAll());
    }

    @Test
    public void updateTest() {
        Image image = new Image();
        image.setName("test");

        Integer id = entityManager.persistAndGetId(image, Integer.class);
        entityManager.detach(image);

        image.setName("test2");
        imageRepository.save(image);

        assertEquals(image, imageRepository.findOne(id));
    }

    @Test
    public void deleteTest() {
        Image image = new Image();
        image.setName("test");

        Integer id = entityManager.persistAndGetId(image, Integer.class);
        entityManager.detach(image);

        imageRepository.delete(id);
        assertNull(imageRepository.findOne(id));
    }
}
