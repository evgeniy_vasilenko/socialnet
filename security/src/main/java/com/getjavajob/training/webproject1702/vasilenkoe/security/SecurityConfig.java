package com.getjavajob.training.webproject1702.vasilenkoe.security;

import com.getjavajob.training.webproject1702.vasilenkoe.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccountService accountService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic();

        http.formLogin()
                .permitAll()
                .loginPage("/")
                .loginProcessingUrl("/loginProcess")
                .failureForwardUrl("/");

        http.authorizeRequests()
                .antMatchers("/account/**", "/group/**")
                .hasAnyAuthority("ROLE_USER", "ROLE_ADMIN");

        http.logout().logoutSuccessUrl("/");
        http.csrf().disable();
        http.rememberMe().key("key");
    }

    @Override
    @Bean
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return super.userDetailsServiceBean();
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(username -> accountService.getByEmail(username)).passwordEncoder(encoder());
    }
}