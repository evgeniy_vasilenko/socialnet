package com.getjavajob.training.webproject1702.vasilenkoe.service;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountFriend;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountFriendRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.ImageRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.RoleRepository;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Evgeniy on 03.08.2017.
 */
public interface AccountService extends Service {

    AccountRepository getAccountRepository();

    void setAccountRepository(AccountRepository accountRepository);

    AccountFriendRepository getAccountFriendRepository();

    void setAccountFriendRepository(AccountFriendRepository accountFriendRepository);

    ImageRepository getImageRepository();

    void setImageRepository(ImageRepository imageRepository);

    RoleRepository getRoleRepository();

    void setRoleRepository(RoleRepository roleRepository);

    Account create(Account account) throws ServiceException;

    void remove(int id) throws ServiceException;

    Account update(Account account) throws ServiceException;

    List<Account> getByName(String name);

    Account getByEmail(String email);

    Account getById(int id);

    Account uploadAvatar(int accountId, InputStream image) throws ServiceException;

    void updatePassword(Account account) throws ServiceException;

    void updateRole(Account account) throws ServiceException;

    List<Account> getByName(String name, int offset, int limit);

    long getAccountByNameCount(String name);

    AccountFriend sendFriendRequest(AccountFriend accountFriend) throws ServiceException;

    AccountFriend acceptFriendRequest(AccountFriend accountFriend) throws ServiceException;

    void removeFriend(AccountFriend accountFriend) throws ServiceException;

    List<Account> getFriendsRequests(int accId);

    List<Account> getFriends(int accId);

    boolean isFriends(int acc1, int acc2);

    long getFriendsRequestsCount(Account account);
}
