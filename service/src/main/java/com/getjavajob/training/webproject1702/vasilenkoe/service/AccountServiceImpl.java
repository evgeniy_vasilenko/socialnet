package com.getjavajob.training.webproject1702.vasilenkoe.service;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountFriend;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Phone;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountFriendRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.ImageRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.RoleRepository;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Created by Evgeniy on 28.04.2017.
 */
@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private static final String ADMIN = "ROLE_ADMIN";
    private static final String USER = "ROLE_USER";

    private AccountRepository accountRepository;
    private AccountFriendRepository accountFriendRepository;
    private ImageRepository imageRepository;
    private RoleRepository roleRepository;

    @Override
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    @Override
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public AccountFriendRepository getAccountFriendRepository() {
        return accountFriendRepository;
    }

    @Autowired
    @Override
    public void setAccountFriendRepository(AccountFriendRepository accountFriendRepository) {
        this.accountFriendRepository = accountFriendRepository;
    }

    @Override
    public ImageRepository getImageRepository() {
        return imageRepository;
    }

    @Autowired
    @Override
    public void setImageRepository(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Override
    public RoleRepository getRoleRepository() {
        return roleRepository;
    }

    @Autowired
    @Override
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public Account create(Account account) throws ServiceException {
        logger.debug("create {}", account);

        try {
            account.setRole(roleRepository.getByName(USER));

            List<Phone> phones = account.getPhones();

            removeIfEmpty(phones);
            for (Phone phone : phones) {
                phone.setAccount(account);
            }

            return accountRepository.save(account);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public Account update(Account account) throws ServiceException {
        logger.debug("update {}", account);

        try {
            accountRepository.deletePhonesForAccount(account);

            List<Phone> phones = account.getPhones();

            removeIfEmpty(phones);
            for (Phone phone : phones) {
                phone.setAccount(account);
            }

            return accountRepository.saveAndFlush(account);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    private void removeIfEmpty(List<Phone> phones) {
        logger.debug("removeIfEmpty {}", phones);

        phones.removeIf(p -> p.getNumber() == null);
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void remove(int id) throws ServiceException {
        logger.debug("remove {}", id);

        try {
            accountRepository.delete(id);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Account> getByName(String name) {
        logger.debug("getByName {}", name);

        return accountRepository.findByLastNameContainsOrFirstNameContains(name, name);
    }

    @Override
    public Account getByEmail(String email) {
        logger.debug("getByEmail {}", email);

        return accountRepository.getByEmail(email);
    }

    @Override
    public Account getById(int id) {
        logger.debug("getById {}", id);

        return accountRepository.findOne(id);
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public Account uploadAvatar(int accountId, InputStream image) throws ServiceException {
        logger.debug("uploadAvatar {}", accountId);

        try {
            Image img = new Image();
            byte[] bytes = IOUtils.toByteArray(image);
            byte[] base64 = Base64.getEncoder().encode(bytes);
            img.setBase64(new String(base64, "UTF-8"));
            imageRepository.save(img);
            accountRepository.updateImage(accountId, img);

            return accountRepository.findOne(accountId);
        } catch (DataAccessException | IOException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void updatePassword(Account account) throws ServiceException {
        logger.debug("updatePassword {}", account);

        try {
            accountRepository.updatePassword(account);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void updateRole(Account account) throws ServiceException {
        logger.debug("updateRole {}", account);

        try {
            account.setRole(roleRepository.getByName(ADMIN));
            accountRepository.updateRole(account);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Account> getByName(String name, int offset, int limit) {
        logger.debug("getByName {}", name);

        Pageable pageable = new PageRequest(offset, limit);
        return accountRepository.findByLastNameContainsOrFirstNameContains(name, name, pageable);
    }

    @Override
    public long getAccountByNameCount(String name) {
        logger.debug("getAccountByNameCount {}", name);

        return accountRepository.countAccountByLastNameContainsOrFirstNameContains(name, name);
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public AccountFriend sendFriendRequest(AccountFriend accountFriend) throws ServiceException {
        logger.debug("sendFriendRequest {}", accountFriend);

        orderFriends(accountFriend);
        try {
            return accountFriendRepository.save(accountFriend);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public AccountFriend acceptFriendRequest(AccountFriend accountFriend) throws ServiceException {
        logger.debug("acceptFriendRequest {}", accountFriend);

        orderFriends(accountFriend);
        try {
            AccountFriend accFrnd = accountFriendRepository.getAccountFriend(accountFriend);
            accFrnd.setAccepted(true);
            return accountFriendRepository.save(accFrnd);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void removeFriend(AccountFriend accountFriend) throws ServiceException {
        logger.debug("removeFriend {}", accountFriend);

        orderFriends(accountFriend);
        try {
            AccountFriend accFrnd = accountFriendRepository.getAccountFriend(accountFriend);
            accountFriendRepository.delete(accFrnd);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    private void orderFriends(AccountFriend accountFriend) {
        logger.debug("orderFriends {}", accountFriend);

        Account account1 = accountFriend.getAcc1();
        Account account2 = accountFriend.getAcc2();

        if (account1.getId() > account2.getId()) {
            accountFriend.setAcc1(account2);
            accountFriend.setAcc2(account1);
        }
    }

    @Override
    public List<Account> getFriends(int accId) {
        logger.debug("getFriends {}", accId);

        return filterFriends(accId, accountFriendRepository.getAccountFriends(accId));
    }

    @Override
    public List<Account> getFriendsRequests(int accId) {
        logger.debug("getFriendsRequests {}", accId);

        List<Account> friends = new ArrayList<>();

        for (AccountFriend accFrnd : accountFriendRepository.getAccountFriends(accId)) {
            if (!accFrnd.isAccepted() && accFrnd.getRequestReceiver().getId() == accId) {
                findAndAdd(accId, friends, accFrnd);
            }
        }

        return friends;
    }

    private List<Account> filterFriends(int accId, List<AccountFriend> accountFriends) {
        List<Account> friends = new ArrayList<>();

        for (AccountFriend accFrnd : accountFriends) {
            if (accFrnd.isAccepted()) {
                findAndAdd(accId, friends, accFrnd);
            }
        }

        return friends;
    }

    /**
     * Selects an account that is a friend and adds to the list
     *
     * @param accId
     * @param friends
     * @param accFrnd
     */
    private void findAndAdd(int accId, List<Account> friends, AccountFriend accFrnd) {
        logger.debug("findAndAdd {} {} {}", accId, friends, accFrnd);

        if (accFrnd.getAcc1().getId() != accId) {
            friends.add(accFrnd.getAcc1());
        } else {
            friends.add(accFrnd.getAcc2());
        }
    }

    @Override
    public boolean isFriends(int acc1, int acc2) {
        logger.debug("isFriends {} {}", acc1, acc2);

        List<AccountFriend> accountFriends = accountFriendRepository.getAccountFriends(acc1);

        return accountFriends.stream().anyMatch(p -> p.getAcc1().getId() == acc2 || p.getAcc2().getId() == acc2);
    }

    @Override
    public long getFriendsRequestsCount(Account account) {

        return accountFriendRepository.countAccountFriendByRequestReceiverEqualsAndAcceptedFalse(account);
    }
}
