package com.getjavajob.training.webproject1702.vasilenkoe.service;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountGroup;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.common.GroupRole;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.*;

import java.util.List;

/**
 * Created by Evgeniy on 03.08.2017.
 */
public interface GroupService extends Service {

    GroupRepository getGroupRepository();

    void setGroupRepository(GroupRepository groupRepository);

    AccountGroupRepository getAccountGroupRepository();

    void setAccountGroupRepository(AccountGroupRepository accountGroupRepository);

    GroupRoleRepository getGroupRoleRepository();

    void setGroupRoleRepository(GroupRoleRepository groupRoleRepository);

    AccountRepository getAccountRepository();

    void setAccountRepository(AccountRepository accountRepository);

    ImageRepository getImageRepository();

    void setImageRepository(ImageRepository imageRepository);

    AccountGroup create(AccountGroup accountGroup) throws ServiceException;

    Group update(Group group) throws ServiceException;

    List<Group> getByName(String groupName);

    Group getById(int id);

    GroupRole getAccountRoleForGroup(Account account, int groupId);

    List<AccountGroup> getGroupMemberRequests(int groupId);

    void sendMemberRequest(int accId, int groupId) throws ServiceException;

    void updateMember(int accId, int groupId, int role) throws ServiceException;

    void deleteMember(int accId, int groupId) throws ServiceException;

    List<Group> getByName(String name, int offset, int limit);

    long getGroupByNameCount(String name);

    AccountGroup getAccountGroupById(int id);

    GroupRole getGroupRole(int accId, int groupId);
}
