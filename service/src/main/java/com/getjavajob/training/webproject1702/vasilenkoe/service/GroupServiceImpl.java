package com.getjavajob.training.webproject1702.vasilenkoe.service;

import com.getjavajob.training.webproject1702.vasilenkoe.common.*;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Evgeniy on 28.04.2017.
 */
@Service
public class GroupServiceImpl implements GroupService {

    private static final Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);

    private static final String AUTHOR = "AUTHOR";
    private static final String MEMBER = "MEMBER";
    private static final String NOT_APPROVED = "NOT_APPROVED";

    private GroupRepository groupRepository;
    private AccountGroupRepository accountGroupRepository;
    private GroupRoleRepository groupRoleRepository;
    private AccountRepository accountRepository;
    private ImageRepository imageRepository;

    @Override
    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    @Autowired
    @Override
    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public AccountGroupRepository getAccountGroupRepository() {
        return accountGroupRepository;
    }

    @Autowired
    @Override
    public void setAccountGroupRepository(AccountGroupRepository accountGroupRepository) {
        this.accountGroupRepository = accountGroupRepository;
    }

    @Override
    public GroupRoleRepository getGroupRoleRepository() {
        return groupRoleRepository;
    }

    @Autowired
    @Override
    public void setGroupRoleRepository(GroupRoleRepository groupRoleRepository) {
        this.groupRoleRepository = groupRoleRepository;
    }

    @Override
    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Autowired
    @Override
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public ImageRepository getImageRepository() {
        return imageRepository;
    }

    @Autowired
    @Override
    public void setImageRepository(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public AccountGroup create(AccountGroup accountGroup) throws ServiceException {
        logger.debug("create {}", accountGroup);

        try {
            accountGroup.setRole(groupRoleRepository.getByName(AUTHOR));
            accountGroupRepository.save(accountGroup);

            return accountGroup;
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public Group update(Group group) throws ServiceException {
        logger.debug("update {}", group);

        try {
            return groupRepository.saveAndFlush(group);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Group> getByName(String groupName) {
        logger.debug("getByName {}", groupName);

        return groupRepository.getByName(groupName);
    }

    @Override
    public Group getById(int id) {
        logger.debug("getById {}", id);

        return groupRepository.findOne(id);
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public Group uploadAvatar(int groupId, InputStream image) throws ServiceException {
        logger.debug("uploadAvatar {}", groupId);

        try {
            Image img = new Image();
            byte[] bytes = IOUtils.toByteArray(image);
            byte[] bas64 = Base64.getEncoder().encode(bytes);
            img.setBase64(new String(bas64, "UTF-8"));
            imageRepository.save(img);
            groupRepository.updateImage(groupId, img);

            return groupRepository.findOne(groupId);
        } catch (DataAccessException | IOException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Override
    public GroupRole getAccountRoleForGroup(Account account, int groupId) {
        logger.debug("getAccountRoleForGroup {} {}", account, groupId);

        Optional<AccountGroup> accountGroupOptional = account.getAccountGroups()
                .stream().filter(p -> p.getGroup().getId() == groupId).findFirst();

        return accountGroupOptional.map(AccountGroup::getRole).orElse(null);
    }

    @Override
    public List<AccountGroup> getGroupMemberRequests(int groupId) {
        logger.debug("getGroupMemberRequests {}", groupId);

        Group group = groupRepository.findOne(groupId);
        GroupRole notApproved = groupRoleRepository.getByName(NOT_APPROVED);

        return group.getAccountGroups()
                .stream().filter(p -> p.getRole().equals(notApproved))
                .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void sendMemberRequest(int accId, int groupId) throws ServiceException {
        logger.debug("sendMemberRequest {} {}", accId, groupId);

        try {
            Account account = accountRepository.findOne(accId);
            Group group = groupRepository.findOne(groupId);
            AccountGroup accountGroup = new AccountGroup();
            accountGroup.setAccount(account);
            accountGroup.setRole(groupRoleRepository.getByName(NOT_APPROVED));
            group.addToAccountGroups(accountGroup);

            accountGroupRepository.saveAndFlush(accountGroup);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void updateMember(int accId, int groupId, int roleId) throws ServiceException {
        logger.debug("updateMember {} {}", accId, groupId);

        try {
            Group group = groupRepository.findOne(groupId);

            AccountGroup accountGroup = group.getAccountGroups()
                    .stream().filter(p -> accId == p.getAccount().getId()).findFirst().get();

            accountGroup.setRole(groupRoleRepository.findOne(roleId));
            accountGroupRepository.saveAndFlush(accountGroup);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Transactional(rollbackFor = ServiceException.class)
    @Override
    public void deleteMember(int accId, int groupId) throws ServiceException {
        logger.debug("deleteMember {} {}", accId, groupId);

        try {
            accountGroupRepository.remove(accId, groupId);
        } catch (DataAccessException e) {
            logger.error(e.getMessage());
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Group> getByName(String name, int offset, int limit) {
        logger.debug("getByName {}", name);

        return groupRepository.findByNameContainsOrDirectionContains(name, name, new PageRequest(offset, limit));
    }

    @Override
    public long getGroupByNameCount(String name) {
        logger.debug("getAccountByNameCount {}", name);

        return groupRepository.countGroupByNameContainsOrDirectionContains(name, name);
    }

    @Override
    public AccountGroup getAccountGroupById(int id) {
        logger.debug("getAccountGroupById {}", id);

        return accountGroupRepository.getOne(id);
    }

    @Override
    public GroupRole getGroupRole(int accId, int groupId) {
        logger.debug("getGroupRole {}, {}", accId, groupId);

        return groupRoleRepository.getGroupRole(accId, groupId);
    }
}
