package com.getjavajob.training.webproject1702.vasilenkoe.service;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Entity;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Evgeniy on 03.08.2017.
 */
public interface Service {

    <T extends Entity> List<T> getByName(String name) throws ServiceException;

    <T extends Entity> T getById(int id) throws ServiceException;

    <T extends Entity> T uploadAvatar(int id, InputStream image) throws ServiceException;

}
