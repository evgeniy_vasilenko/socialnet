package com.getjavajob.training.webproject1702.vasilenkoe.service;

/**
 * Created by Evgeniy on 22.04.2017.
 */
public class ServiceException extends Exception {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

}
