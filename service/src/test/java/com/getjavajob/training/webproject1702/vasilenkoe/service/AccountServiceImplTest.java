package com.getjavajob.training.webproject1702.vasilenkoe.service;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountFriend;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Phone;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Role;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountFriendRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.ImageRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.RoleRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by Evgeniy on 28.04.2017.
 */
public class AccountServiceImplTest {

    private Account testAccount1;
    private Phone testPhone1;
    private Phone testPhone2;

    private AccountService service;

    private AccountRepository accountRepository;
    private AccountFriendRepository accountFriendRepository;
    private ImageRepository imageRepository;
    private RoleRepository roleRepository;

    @Before
    public void init() {

        testAccount1 = new Account();
        testAccount1.setEmail("ivanov@mail.ru");

        testPhone1 = new Phone();
        testPhone1.setNumber("123");
        testPhone2 = new Phone();
        testPhone2.setNumber("321");
        List<Phone> phones = new ArrayList<>();
        phones.add(testPhone1);
        phones.add(testPhone2);
        testAccount1.setPhones(phones);

        accountRepository = mock(AccountRepository.class);
        accountFriendRepository = mock(AccountFriendRepository.class);
        imageRepository = mock(ImageRepository.class);
        roleRepository = mock(RoleRepository.class);


        service = new AccountServiceImpl();
        service.setAccountRepository(accountRepository);
        service.setAccountFriendRepository(accountFriendRepository);
        service.setImageRepository(imageRepository);
        service.setRoleRepository(roleRepository);
    }

    @Test
    public void createAccountTest() throws ServiceException {

        doAnswer(invocation -> {
            Role role = new Role();
            role.setId(1);
            role.setName("ROLE_USER");
            return role;
        }).when(roleRepository).getByName(anyString());

        doAnswer(invocation -> {
            testAccount1.setId(getRandomId());
            return testAccount1;
        }).when(accountRepository).save(any(Account.class));

        Account actualAccount = service.create(testAccount1);

        assertEquals(testAccount1, actualAccount);
        assertTrue(actualAccount.getId() > 0);
        verify(roleRepository).getByName(anyString());
        verify(accountRepository).save(testAccount1);
    }

    @Test
    public void removeTest() throws ServiceException {

        service.remove(anyInt());

        verify(accountRepository).delete(anyInt());
    }

    @Test
    public void updateAccountTest() throws ServiceException {
        doAnswer(invocation -> {
            Account account = new Account();
            account.setFirstName("ivan");
            return account;
        }).when(accountRepository).saveAndFlush(any(Account.class));

        String name = "ivan";
        testAccount1.setFirstName(name);
        testAccount1.setId(1);

        Account actualAccount = service.update(testAccount1);

        assertEquals(name, actualAccount.getFirstName());
        verify(accountRepository).saveAndFlush(any(Account.class));
    }

    @Test
    public void updatePasswordTest() throws ServiceException {

        int password = 321;
        testAccount1.setPassword(String.valueOf(password));

        service.updatePassword(testAccount1);

        verify(accountRepository).updatePassword(any(Account.class));
    }

    @Test
    public void getAccountByIdTest() {
        doAnswer(getAnswerAccount()).when(accountRepository).findOne(anyInt());

        Account account = service.getById(1);
        assertEquals(testAccount1, account);
        verify(accountRepository).findOne(anyInt());
    }

    @Test
    public void getAccountByNameTest() {
        doAnswer(getAnswerAccounts()).when(accountRepository).
                findByLastNameContainsOrFirstNameContains(anyString(), anyString());

        List<Account> accounts = new ArrayList<>();
        testAccount1.setFirstName("ivan");
        accounts.add(testAccount1);

        List<Account> actualAccounts = service.getByName("ivan");

        assertEquals(accounts, actualAccounts);
        assertEquals(accounts.get(0).getFirstName(), actualAccounts.get(0).getFirstName());
        verify(accountRepository).findByLastNameContainsOrFirstNameContains(anyString(), anyString());
    }

    @Test
    public void getAccountByNameOffsetLimitTest() {
        doAnswer(getAnswerAccounts()).when(accountRepository).
                findByLastNameContainsOrFirstNameContains(anyString(), anyString(), any(Pageable.class));

        List<Account> accounts = new ArrayList<>();
        testAccount1.setFirstName("ivan");
        accounts.add(testAccount1);

        List<Account> actualAccounts = service.getByName("ivan", 1, 2);

        assertEquals(accounts, actualAccounts);
        assertEquals(accounts.get(0).getFirstName(), actualAccounts.get(0).getFirstName());
        verify(accountRepository).
                findByLastNameContainsOrFirstNameContains(anyString(), anyString(), any(Pageable.class));
    }

    @Test
    public void getAccountByNameCountTest() {
        doReturn(1L).when(accountRepository).
                countAccountByLastNameContainsOrFirstNameContains(anyString(), anyString());

        long count = service.getAccountByNameCount("ivan");

        assertEquals(1, count);
        verify(accountRepository).countAccountByLastNameContainsOrFirstNameContains(anyString(), anyString());
    }

    @Test
    public void sendFriendRequestTest() throws ServiceException {
        doAnswer(invocation -> {
            AccountFriend accountFriend = new AccountFriend();
            accountFriend.setId(getRandomId());
            return accountFriend;
        }).when(accountFriendRepository).save(any(AccountFriend.class));

        AccountFriend accountFriend = new AccountFriend();
        Account acc1 = new Account();
        acc1.setId(1);
        accountFriend.setAcc1(acc1);
        Account acc2 = new Account();
        acc2.setId(2);
        accountFriend.setAcc2(acc2);
        accountFriend.setRequestReceiver(acc2);
        AccountFriend accFrnd = service.sendFriendRequest(accountFriend);

        assertTrue(accFrnd.getId() > 0);
        verify(accountFriendRepository).save(any(AccountFriend.class));
    }

    @Test
    public void acceptFriendRequestTest() throws ServiceException {
        doAnswer(invocation -> {
            AccountFriend accountFriend = new AccountFriend();
            accountFriend.setAccepted(true);
            return accountFriend;
        }).when(accountFriendRepository).save(any(AccountFriend.class));

        doAnswer(invocation -> {
            AccountFriend accountFriend = new AccountFriend();
            Account acc1 = new Account();
            acc1.setId(1);
            accountFriend.setAcc1(acc1);
            Account acc2 = new Account();
            acc2.setId(2);
            accountFriend.setAcc2(acc2);
            accountFriend.setRequestReceiver(acc2);
            return accountFriend;
        }).when(accountFriendRepository).getAccountFriend(any(AccountFriend.class));

        AccountFriend accountFriend = new AccountFriend();
        Account acc1 = new Account();
        acc1.setId(1);
        accountFriend.setAcc1(acc1);
        Account acc2 = new Account();
        acc2.setId(2);
        accountFriend.setAcc2(acc2);
        accountFriend.setRequestReceiver(acc2);
        AccountFriend accFrnd = service.acceptFriendRequest(accountFriend);

        assertTrue(accFrnd.isAccepted());
        verify(accountFriendRepository).save(any(AccountFriend.class));
        verify(accountFriendRepository).getAccountFriend(any(AccountFriend.class));
    }

    @Test
    public void removeFriendTest() throws ServiceException {
        doAnswer(invocation -> {
            AccountFriend accountFriend = new AccountFriend();
            Account acc1 = new Account();
            acc1.setId(1);
            accountFriend.setAcc1(acc1);
            Account acc2 = new Account();
            acc2.setId(2);
            accountFriend.setAcc2(acc2);
            accountFriend.setRequestReceiver(acc2);
            return accountFriend;
        }).when(accountFriendRepository).getAccountFriend(any(AccountFriend.class));

        AccountFriend accountFriend = new AccountFriend();
        Account acc1 = new Account();
        acc1.setId(1);
        accountFriend.setAcc1(acc1);
        Account acc2 = new Account();
        acc2.setId(2);
        accountFriend.setAcc2(acc2);
        accountFriend.setRequestReceiver(acc2);

        service.removeFriend(accountFriend);
        verify(accountFriendRepository).delete(any(AccountFriend.class));
        verify(accountFriendRepository).getAccountFriend(any(AccountFriend.class));
    }

    @Test
    public void getFriendsRequestsTest() {
        doAnswer(getAccountsList()).when(accountFriendRepository).getAccountFriends(anyInt());

        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(testAccount1);

        assertEquals(expectedAccounts, service.getFriendsRequests(1));
        verify(accountFriendRepository).getAccountFriends(anyInt());
    }

    @Test
    public void getFriendsTest() {
        doAnswer(getAccountsList()).when(accountFriendRepository).getAccountFriends(anyInt());

        List<Account> expectedAccounts = new ArrayList<>();

        assertEquals(expectedAccounts, service.getFriends(1));
        verify(accountFriendRepository).getAccountFriends(anyInt());
    }

    private Answer<List<AccountFriend>> getAccountsList() {
        return invocation -> {
            List<AccountFriend> accounts = new ArrayList<>();
            AccountFriend accountFriend = new AccountFriend();
            testAccount1.setId(1);
            accountFriend.setAcc1(testAccount1);
            accountFriend.setAcc2(testAccount1);
            accountFriend.setRequestReceiver(testAccount1);
            accountFriend.setAccepted(false);
            accounts.add(accountFriend);
            return accounts;
        };
    }

    @Test
    public void getFriendsRequestsCountTest() {
        doReturn(1L).when(accountFriendRepository)
                .countAccountFriendByRequestReceiverEqualsAndAcceptedFalse(any(Account.class));

        assertEquals(1L, service.getFriendsRequestsCount(new Account()));

        verify(accountFriendRepository).countAccountFriendByRequestReceiverEqualsAndAcceptedFalse(any(Account.class));
    }

    @Test
    public void updateRoleTest() throws ServiceException {

        service.updateRole(new Account());

        verify(roleRepository).getByName(anyString());
        verify(accountRepository).updateRole(any(Account.class));
    }

    @Test
    public void getByEmailTest() {
        doAnswer(getAnswerAccount()).when(accountRepository).getByEmail(anyString());

        Account account = service.getByEmail("ivanov@mail.ru");
        assertEquals(testAccount1, account);
        verify(accountRepository).getByEmail(anyString());
    }

    private int getRandomId() {
        return (int) (Math.random() * 10) + 1;
    }

    private Answer getAnswerAccount() {
        return invocation -> {
            Account account = new Account();
            account.setEmail("ivanov@mail.ru");
            return account;
        };
    }

    private Answer getAnswerAccounts() {
        return invocation -> {
            Account account = new Account();
            account.setEmail("ivanov@mail.ru");
            account.setFirstName("ivan");

            List<Account> accounts = new ArrayList<>();
            accounts.add(account);

            return accounts;
        };
    }
}
