package com.getjavajob.training.webproject1702.vasilenkoe.service;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountGroup;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.common.GroupRole;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountGroupRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.AccountRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.GroupRepository;
import com.getjavajob.training.webproject1702.vasilenkoe.dao.GroupRoleRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by Evgeniy on 28.04.2017.
 */
public class GroupServiceImplTest {

    private Account testAccount1;

    private Group testGroup1;

    private GroupService service;

    private GroupRepository groupRepository;
    private GroupRoleRepository groupRoleRepository;
    private AccountRepository accountRepository;
    private AccountGroupRepository accountGroupRepository;

    @Before
    public void init() {

        testAccount1 = new Account();
        testAccount1.setEmail("ivanov@mail.ru");

        testGroup1 = new Group();
        testGroup1.setName("chess");

        groupRepository = mock(GroupRepository.class);
        groupRoleRepository = mock(GroupRoleRepository.class);
        accountRepository = mock(AccountRepository.class);
        accountGroupRepository = mock(AccountGroupRepository.class);

        service = new GroupServiceImpl();
        service.setGroupRepository(groupRepository);
        service.setGroupRoleRepository(groupRoleRepository);
        service.setAccountRepository(accountRepository);
        service.setAccountGroupRepository(accountGroupRepository);
    }

    @Test
    public void createGroupTest() throws ServiceException {
        doAnswer(invocation -> {
            Group group = new Group();
            group.setId(getRandomId());
            group.setName("chess");
            return group;
        }).when(groupRepository).save(any(Group.class));

        AccountGroup accountGroup = new AccountGroup();

        doAnswer(invocation -> {
            accountGroup.setId(getRandomId());
            return accountGroup;
        }).when(accountGroupRepository).save(accountGroup);

        service.create(accountGroup);

        assertTrue(accountGroup.getId() > 0);

        verify(accountGroupRepository).save(any(AccountGroup.class));
    }

    @Test
    public void getGroupByNameTest() {
        doAnswer(invocation -> {
            List<Group> groups = new ArrayList<>();
            groups.add(testGroup1);
            return groups;
        }).when(groupRepository).getByName(anyString());

        List<Group> groups = new ArrayList<>();
        groups.add(testGroup1);

        assertEquals(groups, service.getByName("chess"));
        verify(groupRepository).getByName(anyString());
    }

    @Test
    public void getGroupByIdTest() {
        doAnswer(invocation -> {
            Group group = new Group();
            group.setName("chess");
            return group;
        }).when(groupRepository).findOne(anyInt());

        assertEquals(testGroup1, service.getById(1));

        verify(groupRepository).findOne(anyInt());
    }

    @Test
    public void updateGroupTest() throws ServiceException {
        doAnswer(invocation -> {
            Group group = new Group();
            group.setName("football");
            return group;
        }).when(groupRepository).saveAndFlush(any(Group.class));

        testGroup1.setId(1);
        testGroup1.setName("football");

        assertEquals(testGroup1, service.update(testGroup1));

        verify(groupRepository).saveAndFlush(any(Group.class));
    }

    @Test
    public void sendMemberRequestTest() throws ServiceException {
        doAnswer(invocation -> new Account()).when(accountRepository).findOne(anyInt());
        doAnswer(invocation -> new Group()).when(groupRepository).findOne(anyInt());
        doAnswer(invocation -> new Group()).when(groupRepository).saveAndFlush(any(Group.class));
        doAnswer(invocation -> new GroupRole()).when(groupRoleRepository).getByName(anyString());

        service.sendMemberRequest(1, 1);

        verify(accountRepository).findOne(anyInt());
        verify(groupRepository).findOne(anyInt());
        verify(accountGroupRepository).saveAndFlush(any(AccountGroup.class));
    }

    private int getRandomId() {
        return (int) (Math.random() * 10) + 1;
    }
}
