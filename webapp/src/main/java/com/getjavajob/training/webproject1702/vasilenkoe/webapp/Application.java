package com.getjavajob.training.webproject1702.vasilenkoe.webapp;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.getjavajob.training.webproject1702.vasilenkoe.dao"})
@EntityScan(basePackages = {"com.getjavajob.training.webproject1702.vasilenkoe.common"},
        basePackageClasses = {Application.class, Jsr310JpaConverters.class})
@ComponentScan(basePackages = {"com.getjavajob.training.webproject1702.vasilenkoe.service",
        "com.getjavajob.training.webproject1702.vasilenkoe.security",
        "com.getjavajob.training.webproject1702.vasilenkoe.webapp"})
public class Application {

    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}