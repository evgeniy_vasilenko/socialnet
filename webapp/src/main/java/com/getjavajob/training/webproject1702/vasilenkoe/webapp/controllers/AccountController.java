package com.getjavajob.training.webproject1702.vasilenkoe.webapp.controllers;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountFriend;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountGroup;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.service.AccountService;
import com.getjavajob.training.webproject1702.vasilenkoe.service.ServiceException;
import com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto.AccountAutocompleteDto;
import com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto.AccountDto;
import com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto.AccountXmlDto;
import com.getjavajob.training.webproject1702.vasilenkoe.webapp.enums.FriendsStatus;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.webproject1702.vasilenkoe.webapp.utils.AccountGroupUtils.getDataTablesResponse;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created by Evgeniy on 28.05.2017.
 */
@RestController
@RequestMapping(value = "/account/", produces = APPLICATION_JSON_UTF8_VALUE)
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private static final int AUTOCOMPLETE_OFFSET = 0;
    private static final int AUTOCOMPLETE_MAX_RESULTS = 9;
    private AccountService accountService;
    private ModelMapper modelMapper;
    private PasswordEncoder encoder;

    @Autowired
    public AccountController(AccountService accountService, ModelMapper modelMapper, PasswordEncoder encoder) {
        this.accountService = accountService;
        this.modelMapper = modelMapper;
        this.encoder = encoder;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<AccountDto> authenticated(@AuthenticationPrincipal Account authenticated) {

        logger.debug("account/{}", authenticated);

        return ResponseEntity.ok(modelMapper.map(authenticated, AccountDto.class));
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<AccountDto> account(@PathVariable(required = false) Integer id) {

        logger.debug("account/{}", id);

        Account account = accountService.getById(id);
        return ResponseEntity.ok(modelMapper.map(account, AccountDto.class));
    }

    @RequestMapping(value = "update", method = RequestMethod.PUT)
    public ResponseEntity<Void> doUpdateAccount(@AuthenticationPrincipal Account authenticated,
                                                @ModelAttribute Account account) {

        logger.debug("/account/update {}", account);

        try {
            account.setId(authenticated.getId());
            accountService.update(account);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "update/password", method = RequestMethod.PUT)
    public ResponseEntity<Void> doUpdatePassword(@AuthenticationPrincipal Account authenticated,
                                                 @RequestParam("password") String password) {

        logger.debug("/account/update/password {}", authenticated);

        authenticated.setPassword(encoder.encode(password));
        try {
            accountService.updatePassword(authenticated);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "update/avatar", method = RequestMethod.PUT)
    public ResponseEntity<Void> uploadAccountAvatar(@AuthenticationPrincipal Account authenticated,
                                                    @RequestParam("entity") MultipartFile filePart) throws IOException {

        logger.debug("/account/update/avatar {}", authenticated);

        InputStream fileContent = filePart.getInputStream();
        try {
            accountService.uploadAvatar(authenticated.getId(), fileContent);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "friends/requests", method = RequestMethod.GET)
    public ResponseEntity<String> requestsFriends(@AuthenticationPrincipal Account authenticated,
                                                  @RequestParam(value = "sEcho") String sEcho) {

        logger.debug("/account/friends/requests {}", authenticated);

        List<Account> accounts = accountService.getFriendsRequests(authenticated.getId());
        return getDataTablesResponse(sEcho, 0, accounts, false);
    }

    @RequestMapping(value = "friends/requests/accept/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> requestFriendAccept(@AuthenticationPrincipal Account authenticated,
                                                    @PathVariable("id") int id) {

        logger.debug("/account/friends/requests/accept/ {}", id);

        AccountFriend accountFriend = new AccountFriend();
        try {
            accountFriend.setAcc1(authenticated);
            accountFriend.setAcc2(accountService.getById(id));
            accountFriend.setAccepted(true);
            accountService.acceptFriendRequest(accountFriend);

            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "friends/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getFriends(@PathVariable(value = "id") int id,
                                             @RequestParam(value = "sEcho") String sEcho) {

        logger.debug("/account/friends/ {}", id);

        List<Account> accounts = accountService.getFriends(id);
        return getDataTablesResponse(sEcho, 0, accounts, false);
    }

    @RequestMapping(value = "friends/requests/send/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> requestFriendSend(@AuthenticationPrincipal Account authenticated,
                                                  @PathVariable(value = "id") int id) {

        logger.debug("/account/friends/requests/send/ {}", id);

        AccountFriend accountFriend = new AccountFriend();
        try {
            accountFriend.setAcc1(authenticated);
            Account requestReceiver = accountService.getById(id);
            accountFriend.setAcc2(requestReceiver);
            accountFriend.setRequestReceiver(requestReceiver);
            accountService.sendFriendRequest(accountFriend);

            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "friends/remove/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> removeFriend(@AuthenticationPrincipal Account authenticated,
                                             @PathVariable(value = "id") int id) {

        logger.debug("/account/friends/remove/ {}", id);

        AccountFriend accountFriend = new AccountFriend();
        try {
            accountFriend.setAcc1(authenticated);
            accountFriend.setAcc2(accountService.getById(id));
            accountService.removeFriend(accountFriend);

            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "friends/isFriends/{id}", method = RequestMethod.GET)
    public ResponseEntity<FriendsStatus> isFriends(@AuthenticationPrincipal Account authenticated,
                                                   @PathVariable("id") int id) {

        logger.debug("/account/friends/isFriends/{}", id);

        int authAccId = authenticated.getId();
        FriendsStatus status;
        if (id == authAccId) {
            status = FriendsStatus.SELF_ACCOUNT;
        } else {
            status = accountService.isFriends(authAccId, id) ? FriendsStatus.FRIENDS : FriendsStatus.NOT_FRIENDS;
        }
        return ResponseEntity.ok(status);
    }

    @RequestMapping(value = "xml/export", method = RequestMethod.GET)
    public ResponseEntity<byte[]> exportXml(@AuthenticationPrincipal Account authenticated,
                                            HttpServletResponse response) {

        logger.debug("/account/xml/export {}", authenticated);

        XStream xStream = new XStream(new DomDriver());
        AccountXmlDto accountXmlDto = convertToDto(authenticated);
        String xml = xStream.toXML(accountXmlDto);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=account.xml");

        return ResponseEntity.ok(xml.getBytes());
    }

    private AccountXmlDto convertToDto(Account account) {
        return modelMapper.map(account, AccountXmlDto.class);
    }

    @RequestMapping(value = "xml/import", method = RequestMethod.PUT)
    public ResponseEntity<Void> importXml(@AuthenticationPrincipal Account authenticated,
                                          @RequestParam("entity") MultipartFile filePart) throws IOException, SAXException, ServletException {

        logger.debug("/account/xml/import {}", authenticated);

        XStream xStream = new XStream(new DomDriver());
        InputStream fileContent = filePart.getInputStream();

        if (isValid(fileContent)) {
            AccountXmlDto accountXmlDto = (AccountXmlDto) xStream.fromXML(fileContent);
            Account account = convertToEntity(accountXmlDto);
            account.setId(authenticated.getId());
            try {
                accountService.update(account);
                return ResponseEntity.ok().build();
            } catch (ServiceException e) {
                logger.error(e.getMessage());
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    private boolean isValid(InputStream inputStream) throws SAXException, IOException {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        URL schemaUrl = Thread.currentThread().getContextClassLoader().getResource("account-schema.xsd");
        Schema schema = schemaFactory.newSchema(schemaUrl);
        Validator validator = schema.newValidator();
        inputStream.mark(0);
        try {
            validator.validate(new StreamSource(inputStream));
        } catch (SAXException e) {
            return false;
        }
        inputStream.reset();
        return true;
    }

    private Account convertToEntity(AccountXmlDto accountXmlDto) {
        return modelMapper.map(accountXmlDto, Account.class);
    }

    @RequestMapping(value = "friends/requests/count", method = RequestMethod.GET)
    public ResponseEntity<Long> getFriendsRequestsCount(@AuthenticationPrincipal Account authenticated) {

        logger.debug("/account/friends/requests/count {}", authenticated);

        return ResponseEntity.ok(accountService.getFriendsRequestsCount(authenticated));
    }

    @RequestMapping(value = "/groups/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getGroups(@PathVariable int id,
                                            @RequestParam(value = "sEcho") String sEcho) {

        logger.debug("account/groups/ {}", id);

        List<Group> groups = new ArrayList<>();
        for (AccountGroup accountGroup : accountService.getById(id).getAccountGroups()) {
            groups.add(accountGroup.getGroup());
        }
        return getDataTablesResponse(sEcho, 0, groups, false);
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public ResponseEntity<String> searchAccounts(@RequestParam(value = "entity") String entity,
                                                 @RequestParam(value = "sEcho") String sEcho,
                                                 @RequestParam(value = "iDisplayLength") int iDisplayLength,
                                                 @RequestParam(value = "iDisplayStart") int iDisplayStart) throws ServletException, IOException {

        logger.debug("/account/search {}", entity);

        int iTotalDisplayRecords = (int) accountService.getAccountByNameCount(entity);
        List<Account> accounts = accountService.getByName(entity, iDisplayStart / iDisplayLength, iDisplayLength);

        return getDataTablesResponse(sEcho, iTotalDisplayRecords, accounts, true);
    }

    @RequestMapping(value = "search/autocomplete", method = RequestMethod.GET)
    public ResponseEntity<List<AccountAutocompleteDto>> searchAutocomplete(@RequestParam("entity") String entity) {

        logger.debug("/account/search/autocomplete {}", entity);

        List<AccountAutocompleteDto> autocompleteDtos =
                convertToDtoList(accountService.getByName(entity, AUTOCOMPLETE_OFFSET, AUTOCOMPLETE_MAX_RESULTS));

        return ResponseEntity.ok(autocompleteDtos);
    }

    private List<AccountAutocompleteDto> convertToDtoList(List<Account> accounts) {

        List<AccountAutocompleteDto> dtoList = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();
        for (Account account : accounts) {
            dtoList.add(modelMapper.map(account, AccountAutocompleteDto.class));
        }
        return dtoList;
    }
}

