package com.getjavajob.training.webproject1702.vasilenkoe.webapp.controllers;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.AccountGroup;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.common.GroupRole;
import com.getjavajob.training.webproject1702.vasilenkoe.service.GroupService;
import com.getjavajob.training.webproject1702.vasilenkoe.service.ServiceException;
import com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto.GroupDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.webproject1702.vasilenkoe.webapp.utils.AccountGroupUtils.getDataTablesResponse;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created by Evgeniy on 30.05.2017.
 */
@RestController
@RequestMapping(value = "/group/", produces = APPLICATION_JSON_UTF8_VALUE)
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    private GroupService groupService;
    private ModelMapper modelMapper;

    @Autowired
    public GroupController(GroupService groupService, ModelMapper modelMapper) {
        this.groupService = groupService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<GroupDto> groupView(@PathVariable int id) {

        logger.debug("group {}", id);

        return ResponseEntity.ok(modelMapper.map(groupService.getById(id), GroupDto.class));
    }

    @RequestMapping(value = "registration", method = RequestMethod.POST)
    public ResponseEntity<Void> doRegistrationGroup(@AuthenticationPrincipal Account authenticated,
                                                    @ModelAttribute Group group) {

        logger.debug("/group/registration {}", authenticated);

        AccountGroup accountGroup = new AccountGroup();
        accountGroup.setGroup(group);
        accountGroup.setAccount(authenticated);

        try {
            groupService.create(accountGroup);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.PUT)
    public ResponseEntity<Void> doUpdateGroup(@ModelAttribute Group group) {

        logger.debug("/group/update {}", group);

        try {
            groupService.update(group);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "checkRole/{id}", method = RequestMethod.GET)
    public ResponseEntity<GroupRole> checkRole(@PathVariable(value = "id") int id,
                                               @AuthenticationPrincipal Account authenticated) {

        logger.debug("/group/checkRole/ {}", id);

        return ResponseEntity.ok(groupService.getGroupRole(authenticated.getId(), id));
    }

    @RequestMapping(value = "members/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> groupMembers(@PathVariable(value = "id") int id,
                                               @RequestParam(value = "sEcho") String sEcho) {

        logger.debug("/group/members/ {}", id);

        List<Account> accounts = new ArrayList<>();
        for (AccountGroup accountGroup : groupService.getById(id).getAccountGroups()) {
            accounts.add(accountGroup.getAccount());
        }
        return getDataTablesResponse(sEcho, 0, accounts, false);
    }

    @RequestMapping(value = "member/requests/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> requestsMembers(@PathVariable(value = "id") int id,
                                                  @RequestParam(value = "sEcho") String sEcho) {

        logger.debug("/group/members/requests/ {}", id);

        List<Account> accounts = new ArrayList<>();
        for (AccountGroup accountGroup : groupService.getGroupMemberRequests(id)) {
            accounts.add(accountGroup.getAccount());
        }
        return getDataTablesResponse(sEcho, 0, accounts, false);
    }

    @RequestMapping(value = "member/delete", method = RequestMethod.GET)
    public ResponseEntity<Void> deleteMember(@RequestParam("accId") int accId,
                                             @RequestParam("groupId") int groupId) {

        logger.debug("deleteMember account {}, id {}", accId, groupId);

        return deleteGroupMember(accId, groupId);
    }

    @RequestMapping(value = "member/roleUpdate", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateRoleMember(@RequestParam("accId") int accId,
                                                 @RequestParam("groupId") int groupId,
                                                 @RequestParam("roleId") int role) {

        logger.debug("/group/members/role/update account {}, id {}", accId, groupId);

        try {
            groupService.updateMember(accId, groupId, role);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "member/send/{id}", method = RequestMethod.GET)
    public ResponseEntity<Void> sendRequestMember(@AuthenticationPrincipal Account authenticated,
                                                  @PathVariable("id") int groupId) {

        int accId = authenticated.getId();

        logger.debug("/group/member/send account {}, id {}", accId, groupId);

        try {
            groupService.sendMemberRequest(accId, groupId);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "leave/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> leaveGroup(@AuthenticationPrincipal Account authenticated,
                                           @PathVariable("id") int groupId) {

        int accId = authenticated.getId();

        logger.debug("/group/leave account {}, id {}", accId, groupId);

        return deleteGroupMember(accId, groupId);
    }

    private ResponseEntity<Void> deleteGroupMember(@RequestParam("member") int accId, @RequestParam("id") int groupId) {
        try {
            groupService.deleteMember(accId, groupId);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "update/avatar/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> uploadGroupAvatar(@RequestParam("entity") MultipartFile filePart,
                                                  @PathVariable("id") int id) throws IOException {

        logger.debug("/group/update/avatar/ {}", id);

        InputStream fileContent = filePart.getInputStream();
        try {
            groupService.uploadAvatar(id, fileContent);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "search", method = RequestMethod.GET)
    public ResponseEntity<String> searchGroups(@RequestParam(value = "entity") String entity,
                                               @RequestParam(value = "sEcho") String sEcho,
                                               @RequestParam(value = "iDisplayLength") int iDisplayLength,
                                               @RequestParam(value = "iDisplayStart") int iDisplayStart) {

        logger.debug("searchGroups {}", entity);

        int iTotalDisplayRecords = (int) groupService.getGroupByNameCount(entity);
        List<Group> groups = groupService.getByName(entity, iDisplayStart / iDisplayLength, iDisplayLength);

        return getDataTablesResponse(sEcho, iTotalDisplayRecords, groups, true);
    }
}
