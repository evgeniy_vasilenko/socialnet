package com.getjavajob.training.webproject1702.vasilenkoe.webapp.controllers;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.service.AccountService;
import com.getjavajob.training.webproject1702.vasilenkoe.service.ServiceException;
import com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto.AccountDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;

/**
 * Created by Evjen on 23.09.2017.
 */
@Controller
public class HomepageController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;
    private ModelMapper modelMapper;
    private PasswordEncoder encoder;

    @Autowired
    public HomepageController(AccountService accountService, ModelMapper modelMapper, PasswordEncoder encoder) {
        this.accountService = accountService;
        this.modelMapper = modelMapper;
        this.encoder = encoder;
    }

    @RequestMapping(value = "/")
    public String indexPage() {
        return "main";
    }

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<AccountDto> login(@AuthenticationPrincipal Account authenticated) {

        AccountDto account = authenticated != null ? modelMapper.map(authenticated, AccountDto.class) : null;

        return ResponseEntity.ok(account);
    }

    @ResponseBody
    @RequestMapping(value = "/registration/account", method = RequestMethod.POST)
    public ResponseEntity<Void> doRegistrationAccount(@ModelAttribute Account account) throws ServletException {

        logger.debug("/registration/account {}", account.getEmail());

        account.setPassword(encoder.encode(account.getPassword()));
        try {
            accountService.create(account);

            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }
}
