package com.getjavajob.training.webproject1702.vasilenkoe.webapp.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

import static java.time.LocalDate.parse;

/**
 * Created by Evgeniy on 04.06.2017.
 */
@Component
public class StringToLocaleDateConverter implements Converter<String, LocalDate> {

    @Override
    public LocalDate convert(String birthday) {
        if (birthday != null && birthday.length() > 0) {
            return parse(birthday);
        }

        return null;
    }
}
