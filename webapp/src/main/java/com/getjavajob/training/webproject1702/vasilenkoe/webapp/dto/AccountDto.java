package com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evjen on 11.09.2017.
 */
public class AccountDto {

    private Integer id;

    private String firstName;

    private String middleName;

    private String lastName;

    private LocalDate birthday;

    private String address;

    private String workAddress;

    private String skype;

    private String icq;

    private String hobby;

    private String email;

    private Image image;

    private List<PhoneDto> phones = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<PhoneDto> getPhones() {
        return phones;
    }

    public void setPhones(List<PhoneDto> phones) {
        this.phones = phones;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", address='" + address + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", skype='" + skype + '\'' +
                ", icq='" + icq + '\'' +
                ", hobby='" + hobby + '\'' +
                ", email='" + email + '\'' +
                ", image=" + image +
                ", phones=" + phones +
                '}';
    }
}
