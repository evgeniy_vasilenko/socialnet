package com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;

/**
 * Created by Evjen on 20.09.2017.
 */
public class AccountGroupBridgeDto {

    private Integer id;

    private String name;

    private String info;

    private Image image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "AccountGroupBridgeDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                ", image=" + image +
                '}';
    }
}
