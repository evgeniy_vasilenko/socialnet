package com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto;

import com.getjavajob.training.webproject1702.vasilenkoe.common.GroupRole;

/**
 * Created by Evjen on 18.09.2017.
 */
public class AccountGroupDto {

    private Integer id;

    private AccountDto account;

    private GroupDto group;

    private GroupRole role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AccountDto getAccount() {
        return account;
    }

    public void setAccount(AccountDto account) {
        this.account = account;
    }

    public GroupDto getGroup() {
        return group;
    }

    public void setGroup(GroupDto group) {
        this.group = group;
    }

    public GroupRole getRole() {
        return role;
    }

    public void setRole(GroupRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "AccountGroupDto{" +
                "id=" + id +
                ", account=" + account +
                ", group=" + group +
                ", role=" + role +
                '}';
    }
}
