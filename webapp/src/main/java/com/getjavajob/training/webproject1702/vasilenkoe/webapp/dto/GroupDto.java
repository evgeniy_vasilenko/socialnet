package com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Image;

/**
 * Created by Evjen on 13.09.2017.
 */
public class GroupDto {

    private Integer id;

    private String name;

    private String direction;

    private Image image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "GroupDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", direction='" + direction + '\'' +
                ", image=" + image +
                '}';
    }
}
