package com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto;

/**
 * Created by Evjen on 29.06.2017.
 */
public class PhoneDto {

    private String number;

    private int type;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PhoneDto{" +
                "number='" + number + '\'' +
                ", type=" + type +
                '}';
    }
}
