package com.getjavajob.training.webproject1702.vasilenkoe.webapp.enums;

/**
 * Created by Evjen on 18.09.2017.
 */
public enum FriendsStatus {

    SELF_ACCOUNT,
    NOT_FRIENDS,
    FRIENDS
}
