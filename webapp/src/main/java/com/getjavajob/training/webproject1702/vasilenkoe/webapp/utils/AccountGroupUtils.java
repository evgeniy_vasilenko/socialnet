package com.getjavajob.training.webproject1702.vasilenkoe.webapp.utils;

import com.getjavajob.training.webproject1702.vasilenkoe.common.Account;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Entity;
import com.getjavajob.training.webproject1702.vasilenkoe.common.Group;
import com.getjavajob.training.webproject1702.vasilenkoe.webapp.dto.AccountGroupBridgeDto;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Evjen on 20.09.2017.
 */
public class AccountGroupUtils {

    private static final Logger logger = LoggerFactory.getLogger(AccountGroupUtils.class);

    public static ResponseEntity<String> getDataTablesResponse(String sEcho, int iTotalDisplayRecords,
                                                               List<? extends Entity> entities, boolean pageable) {

        List<AccountGroupBridgeDto> accountGroupBridgeDtos = new ArrayList<>();

        for (Entity entity : entities) {
            accountGroupBridgeDtos.add(convertToAccountGroupBridgeDto(entity));
        }

        JsonArray data = new JsonArray();
        try {
            JsonObject jsonResponse = new JsonObject();
            jsonResponse.addProperty("sEcho", sEcho);
            if (pageable) {
                jsonResponse.addProperty("iTotalDisplayRecords", iTotalDisplayRecords);
            }

            for (AccountGroupBridgeDto dto : accountGroupBridgeDtos) {
                JsonArray row = new JsonArray();
                row.add(new JsonPrimitive(dto.getId()));
                row.add(new JsonPrimitive(dto.getName()));
                row.add(new JsonPrimitive(dto.getInfo()));
                row.add(new JsonPrimitive(dto.getImage().getBase64()));
                data.add(row);
            }

            jsonResponse.add("aaData", data);

            return ResponseEntity.ok(jsonResponse.toString());
        } catch (JsonIOException e) {
            logger.error(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    private static AccountGroupBridgeDto convertToAccountGroupBridgeDto(Entity entity) {

        logger.debug("convertToAccountGroupBridgeDto {}", entity);

        AccountGroupBridgeDto accountGroupBridgeDto = new AccountGroupBridgeDto();

        if (entity instanceof Account) {
            Account account = (Account) entity;

            accountGroupBridgeDto.setId(account.getId());
            accountGroupBridgeDto.setName(account.getFirstName() + " " + account.getLastName());
            accountGroupBridgeDto.setInfo(account.getAddress());
            accountGroupBridgeDto.setImage(account.getImage());
        } else if (entity instanceof Group) {
            Group group = (Group) entity;

            accountGroupBridgeDto.setId(entity.getId());
            accountGroupBridgeDto.setName(group.getName());
            accountGroupBridgeDto.setInfo(group.getDirection());
            accountGroupBridgeDto.setImage(group.getImage());
        } else {
            throw new IllegalArgumentException();
        }

        return accountGroupBridgeDto;
    }

}
