<div class="container hidden" id="account-page">
    <div class="row">
        <div class="col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2"><img class="img-thumbnail img-responsive"
                                                                            id="account-avatar"
                                                                            src=""
                                                                            style="width:262px;">
            <div class="list-group" id="account-buttons">
                <a class="list-group-item list-group-item-info"
                   href="" id="friends"><span
                        class="text-uppercase">friends </span></a>
                <a class="list-group-item list-group-item-info"
                   href="" id="groups"><span
                        class="text-uppercase">groups </span></a>
                <button class="list-group-item list-group-item-danger hidden" id="friend-remove">
                    <span class="text-uppercase">remove friend</span></button>
                <button class="list-group-item list-group-item-success hidden" id="friend-send">
                    <span class="text-uppercase">send friendship request</span></button>
            </div>
        </div>
        <div class="col-lg-5 col-lg-offset-0 col-md-5 col-md-offset-0">
            <div>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Info </a></li>
                    <li><a href="#interests" role="tab" data-toggle="tab">Interests </a></li>
                    <li><a href="#contacts" role="tab" data-toggle="tab">Contacts </a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" role="tabpanel" id="tab-1">
                        <ul class="list-group">
                            <li class="list-group-item" id="name"></li>
                            <li class="list-group-item" id="birthday"></li>
                            <li class="list-group-item" id="address"></li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" role="tabpanel" id="interests">
                        <ul class="list-group">
                            <li class="list-group-item" id="hobby-info"></li>
                        </ul>
                    </div>
                    <div class="tab-pane fade" role="tabpanel" id="contacts">
                        <ul class="list-group">
                            <li class="list-group-item" id="home-phones-info"></li>
                            <li class="list-group-item" id="work-phones-info"></li>
                            <li class="list-group-item">
                                <p id="skype-info"><i class="glyphicon glyphicon-send"></i></p>
                            </li>
                            <li class="list-group-item">
                                <p id="icq-info"><i class="glyphicon glyphicon-send"></i></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>