<div class="container hidden" id="account-update-avatar-page">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <h1>Change avatar</h1>
            <form method="post" enctype="multipart/form-data" id="account-form-avatar"
                  action="${pageContext.request.contextPath}/uploadAccountAvatar?${_csrf.parameterName}=${_csrf.token}">
                <input id="input-id-avatar" class="file" type="file" name="entity" data-preview-file-type="image"
                       data-allowed-file-extensions='["jpg", "png"]'/>
            </form>
        </div>
    </div>
</div>