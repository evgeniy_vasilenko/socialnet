<div class="container hidden" id="account-update-xml-page">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <h1>Export/import XML</h1>
            <h3>Import from XML</h3>
            <form method="post" enctype="multipart/form-data" id="account-form-xml"
                  action="${pageContext.request.contextPath}/account/xml/import">
                <input id="input-id-xml" class="file" type="file" name="entity" data-preview-file-type="text"
                       data-allowed-file-extensions='["xml"]'/>
            </form>
            <h3>Export to XML</h3>
            <a class="btn btn-primary btn-block" id="export-xml"
               href="${pageContext.request.contextPath}/account/xml/export">Export to XML</a>
        </div>
    </div>
</div>

