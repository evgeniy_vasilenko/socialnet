<div class="container hidden" id="group-page">
    <div class="row">
        <div class="col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2"><img class="img-thumbnail img-responsive"
                                                                            id="group-avatar"
                                                                            src=""
                                                                            style="width:262px;">
            <div class="list-group">
                <a class="list-group-item list-group-item-info"
                   href='' id="members">
                    <span class="text-uppercase">members</span></a>
                <button class="list-group-item list-group-item-info hidden" id="member-requests">
                    <span class="text-uppercase">member requests</span></button>
                <button class="list-group-item list-group-item-success hidden" id="group-send">
                    <span class="text-uppercase">send member request</span></button>
                <button class="list-group-item list-group-item-danger hidden" id="group-leave">
                    <span class="text-uppercase">leave group</span></button>
            </div>
        </div>
        <div class="col-lg-5 col-lg-offset-0 col-md-5 col-md-offset-0">
            <div>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Info </a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" role="tabpanel" id="tab-1">
                        <ul class="list-group">
                            <li class="list-group-item" id="group-name"></li>
                            <li class="list-group-item" id="group-direction"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

