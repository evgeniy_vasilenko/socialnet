<div class="container hidden" id="group-registration-page">
    <div class="row">
        <div class="col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
            <h1 class="text-center">Enter group info</h1>
            <form id="group-form">
                <div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="name" placeholder="Group name"
                                   class="form-control" aria-describedby="sizing-addon2"/>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" name="direction" placeholder="Group direction"
                                   class="form-control" aria-describedby="sizing-addon2"/>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success" type="submit" form="group-form" style="margin:14px;"><i
                        class="glyphicon glyphicon-ok"></i></button>
            </form>
        </div>
    </div>
</div>


<%--modals--%>
<%--<div class="modal fade" id="submit-modal" role="dialog">--%>
<%--<div class="modal-dialog">--%>
<%--<div class="modal-content">--%>
<%--<div class="modal-body">--%>
<%--<p>Are you sure about the correctness of the data, complete group registration?</p>--%>
<%--</div>--%>
<%--<div class="modal-footer">--%>
<%--<button type="submit" form="group-form" class="btn btn-default">Submit</button>--%>
<%--<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--<div class="modal fade" id="error-modal" role="dialog">--%>
<%--<div class="modal-dialog">--%>
<%--<div class="modal-content">--%>
<%--<div class="modal-body">--%>
<%--<p>Group name already present</p>--%>
<%--</div>--%>
<%--<div class="modal-footer">--%>
<%--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
