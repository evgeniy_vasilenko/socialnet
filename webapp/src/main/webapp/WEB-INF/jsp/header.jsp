<div class="container hidden" id="header">
    <div class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header"><a href="${pageContext.request.contextPath}/" id="header-logo"
                                                  class="navbar-brand navbar-link">socialnet</a>
                        <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span
                                class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                                class="icon-bar"></span><span class="icon-bar"></span></button>
                    </div>
                    <div class="collapse navbar-collapse" id="navcol-1">
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"
                                                    aria-expanded="false" href="#">Edit account <span
                                    class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li role="presentation"><a href="" id="account-update">Edit info</a></li>
                                    <li role="presentation"><a href="" id="account-update-xml">Export/import xml</a>
                                    </li>
                                    <li role="presentation"><a href="" id="account-update-avatar">Change avatar</a></li>
                                    <li role="presentation"><a href="" id="account-update-password">Change password</a>
                                    </li>
                                </ul>
                            </li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/requestsFriends"
                                                       id="friendship-requests">Friends
                                requests <span id="count" class="badge"></span></a></li>
                            <li role="presentation"><a href="" id="group-registration">Create group</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li role="presentation">
                                <a href="" id="header-name"><strong></strong></a>
                            </li>
                            <li role="presentation">
                                <script>
                                    function formSubmit() {
                                        document.getElementById("logoutForm").submit();
                                    }
                                </script>
                                <a href="javascript:formSubmit()"> Logout</a>
                                <form action="/logout" method="post" id="logoutForm"></form>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right"
                              action="${pageContext.request.contextPath}/searchResults" id="search-form">
                            <div class="form-group">
                                <label class="control-label" for="search-field"><i class="icon ion-search"></i></label>
                                <input class="form-control search-field" type="search" name="entity" id="search-field"/>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>

