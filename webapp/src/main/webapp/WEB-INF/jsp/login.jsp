<div class="container hidden" id="login-form">
    <div class="login-clean">
        <%--<form method="post" action="${pageContext.request.contextPath}/doLogin">--%>
        <form method="post">
            <h2 class="text-center" style="color:#5097cb;">socialnet</h2>
            <h2 class="sr-only">Login Form</h2>

            <div class="illustration" style="color:#5097cb;"><i class="icon ion-happy-outline"></i></div>

            <input class="form-control" type="text" id="j_username" placeholder="Email">

            <input class="form-control" type="password" id="j_password" placeholder="Password">

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember-me" id="remember-me" value="true"/>Remember</label>
            </div>
            <div class="form-group">
                <button id="login_ok" class="btn btn-primary btn-block" type="submit" style="background-color:#5097cb;">
                    Log
                    In
                </button>
            </div>
            <a href="" class="registration" id="account-reg">Not registered yet?! Click here</a>
        </form>
    </div>
</div>
