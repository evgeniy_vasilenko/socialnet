<%--
  Created by IntelliJ IDEA.
  User: Evjen
  Date: 10.09.2017
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>socialnet</title>
    <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/fonts/ionicons.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/fileinput.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dataTables.jqueryui.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datatables.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dataTables.bootstrap.min.css">
    <style>
        .loading {
            background: url('${pageContext.request.contextPath}/resources/img/loading.gif') no-repeat right center;
        }

        table td, table tr, table th {
            background: transparent !important;
        }
    </style>
</head>
<body style="background-color:#f1f4ff;">
<%@ include file="header.jsp" %>
<%@ include file="alert.jsp" %>
<%@ include file="account.jsp" %>
<%@ include file="group.jsp" %>
<%@ include file="login.jsp" %>
<%@ include file="resultsTable.jsp" %>
<%@ include file="accountUpdate.jsp" %>
<%@ include file="accountXml.jsp" %>
<%@ include file="accountAvatarUpload.jsp" %>
<%@ include file="passwordUpdate.jsp" %>
<%@ include file="searchResults.jsp" %>
<%@ include file="groupRegistration.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/dropdown.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/script.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/dataTables.jqueryui.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/fileinput.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/social-network.js"></script>
</body>
</html>
