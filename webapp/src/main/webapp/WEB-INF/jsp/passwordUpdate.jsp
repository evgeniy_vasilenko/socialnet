<div class="container hidden" id="account-update-password-page">
    <div class="row">
        <div class="col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0">
            <h1 class="text-center">Change password</h1>
            <form method="post" id="account-form-password" action="${pageContext.request.contextPath}/doUpdatePassword">
                <div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-sunglasses"></i></span>
                            <input type="password" id="pwd" name="password" placeholder="Enter new password"
                                   class="form-control" aria-describedby="sizing-addon2"/>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-sunglasses"></i></span>
                            <input type="password" id="pwdRepeat" name="pwdRepeat" placeholder="Repeat new password"
                                   class="form-control" aria-describedby="sizing-addon2"/>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success" id="ok-password" type="button" style="margin:14px;"><i
                        class="glyphicon glyphicon-ok"></i></button>
                <input type="hidden" name="id" value="${acc.id}"/>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
    </div>
</div>
</div>

<%--modals--%>
<div class="modal fade" id="submit-password-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Are you sure?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" form="account-form-password" class="btn btn-default">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error-empty-password-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Password can't be empty!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error-password-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Passwords not match!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
