<div class="container hidden" id="search-results-page">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 col-md-12">
            <div>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a href="#accounts-search-tab" role="tab" data-toggle="tab">Accounts </a>
                        </li>
                        <li><a href="#groups-search-tab" role="tab" data-toggle="tab">Groups </a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" role="tabpanel" id="accounts-search-tab">
                            <table id="accounts-table" class="table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>City</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tab-pane fade" role="tabpanel" id="groups-search-tab">
                            <table id="groups-table" class="table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Avatar</th>
                                    <th>Name</th>
                                    <th>Direction</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
