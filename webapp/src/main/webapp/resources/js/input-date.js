/**
 * Created by Evgeniy on 02.06.2017.
 */
$(document).ready(function () {
    $(function () {
        $('#date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });
});
