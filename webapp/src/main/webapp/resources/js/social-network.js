/**
 * Created by Evjen on 23.09.2017.
 */
$(document).ready(function () {
    // login
    $.ajax({
        url: "/login",
        method: "POST",
        success: function (data) {
            if (!data) {
                showPage("#login-form");
            } else {
                showHeader();
                showAcc("/account/");
            }
        }
    });

    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
        if (jqxhr.status == 401) {
            $("#header").addClass("hidden");
            showPage("#login-form");
            showStatus("danger", "invalid credentials or your session expired")
        }
    });

    $("#login-form").submit(function () {
        event.preventDefault();
        var data = 'username=' + $('#j_username').val() + '&password=' + $('#j_password').val();
        if ($('#remember-me').is(':checked')) {
            data += '&remember-me=true';
        }
        $.ajax({
            url: "/loginProcess",
            method: "POST",
            data: data,
            success: function () {
                showHeader();
                showAcc("/account/");
            }
        });
    });

    function showHeader() {
        $.ajax({
            url: "/account/",
            method: "GET",
            success: function (data) {
                $("#header-name").text(data.firstName + " " + data.lastName);
                $("#header").removeClass("hidden");
            }
        })
    }

    $("#header-logo").on("click", function (e) {
        e.preventDefault();
        showAcc("/account/");
    });

    $("#header-name").on("click", function (e) {
        e.preventDefault();
        showAcc("/account/");
    });

    //account
    var update;

    $("#account-reg").on("click", function (e) {
        update = false;
        var markup = "<div id='password-input'>" +
            "<div class='form-group'>" +
            "<div class='input-group'>" +
            "<span class='input-group-addon'>" +
            "<i class='glyphicon glyphicon-sunglasses'></i></span>" +
            "<input type='password' id='pwd' name='password' placeholder='Password, required field' class='form-control' aria-describedby='sizing-addon2'/>" +
            "</div>" +
            "</div>" +
            "</div>";
        $("#email-input").after(markup);
        e.preventDefault();
        showPage("#account-update-page");
    });

    $("#account-update").on("click", function (e) {
        update = true;
        e.preventDefault();
        $.ajax({
            url: "/account/",
            method: "GET",
            success: function (data) {
                $("#account-form-update").remove("#password-input");
                showPage("#account-update-page");
                $("#update-first-name").val(data.firstName);
                $("#update-middle-name").val(data.middleName);
                $("#update-last-name").val(data.lastName);

                if (data.birthday !== null) {

                    var day = data.birthday.dayOfMonth;
                    var month = data.birthday.monthValue - 1;
                    var year = data.birthday.year;
                    var date = new Date(Date.UTC(year, month, day));

                    $("#update-birthday").val(date.toISOString().slice(0, 10));
                }

                $("#update-hobby").val(data.hobby);
                $("#update-skype").val(data.skype);
                $("#update-icq").val(data.icq);
                $("#email").val(data.email);
                showPhones(data.phones);
            }
        })
    });

    $("#account-form").submit(function (e) {
        var postData = $(this).serializeArray();
        var url;
        var method;
        var message;

        if (update) {
            url = "/account/update";
            method = "PUT";
            message = "account data saved";
        } else {
            url = "registration/account";
            method = "POST";
            message = "<p>account created, please</p><a href='#' class='alert-link' onclick=showPage('#login-form')> login</a>"
        }

        $.ajax({
            url: url,
            method: method,
            data: postData,
            success: function (data, textStatus, jqXHR) {
                showStatus("success", message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showStatus("danger", "something wrong");
            }
        });
        e.preventDefault();
        $("#submit-modal").modal("hide");
    });

    $("#account-update-ok").on("click", function () {
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var email = $("#email").val();

        var dialog;

        if (emailRegex.test(email)) {
            dialog = $('#submit-modal');
        } else {
            dialog = $('#error-modal');
        }

        dialog.modal('show');
    });

    $("#account-update-password").on("click", function (e) {
        e.preventDefault();
        showPage("#account-update-password-page");
    });

    $("#account-form-password").submit(function (e) {
        e.preventDefault();
        var postData = $(this).serializeArray();
        $.ajax({
            url: "/account/update/password",
            method: "PUT",
            data: postData,
            success: function (data) {
                showStatus("success", "account data saved");
            },
            error: function (data) {
                showStatus("danger", "something wrong");
            }
        });
        $("#submit-password-modal").modal("hide");
    });

    $("#ok-password").on("click", function () {
        var pwd = $("#pwd").val();
        var pwdRepeat = $("#pwdRepeat").val();

        var dialog;

        if (!pwd || !pwdRepeat) {
            dialog = $('#error-empty-password-modal');
        } else {
            if (pwd === pwdRepeat) {
                dialog = $('#submit-password-modal');
            } else {
                dialog = $('#error-password-modal');
            }
        }

        dialog.modal('show');
    });

    $("#account-update-xml").on("click", function (e) {
        e.preventDefault();
        showPage("#account-update-xml-page");
    });

    $("#account-form-xml").submit(function (e) {
        e.preventDefault();
        var entity = new FormData(this);
        $.ajax({
            url: "/account/xml/import",
            data: entity,
            cache: false,
            contentType: false,
            processData: false,
            method: 'PUT',
            success: function (data) {
                showStatus("success", "account data saved");
            },
            error: function () {
                showStatus("danger", "something wrong");
            }
        });
    });

    $("#account-update-avatar").on("click", function (e) {
        e.preventDefault();
        showPage("#account-update-avatar-page");
    });

    $("#account-form-avatar").submit(function (e) {
        e.preventDefault();
        var entity = new FormData(this);
        $.ajax({
            url: "/account/update/avatar",
            data: entity,
            cache: false,
            contentType: false,
            processData: false,
            method: 'PUT',
            success: function (data) {
                showStatus("success", "account data saved");
            },
            error: function () {
                showStatus("danger", "something wrong");
            }
        });
    });

    //groups
    $("#groups").on("click", function (e) {
        e.preventDefault();
        var table = $("#results-table");
        var source = "/account/groups/" + currentPageData.acc.id;
        var target = "/group/";
        populateTableDisablePagination(table, source, target);
        showPage("#results-page");
    });

    $("#members").on("click", function (e) {
        e.preventDefault();
        var table = $("#results-table");
        var source = "/group/members/" + currentPageData.group.id;
        var target = "/account/";
        populateTableDisablePagination(table, source, target);
        showPage("#results-page");
    });

    $("#member-requests").on("click", function (e) {
        e.preventDefault();
        var table = $("#results-table");
        var source = "/group/member/requests/" + currentPageData.group.id;
        var target = "/account/";
        populateTableDisablePagination(table, source, target);
        showPage("#results-page");
    });

    $("#results-table").on("click", ".btn[name='decline-member']", function () {
        $(this).parent().fadeOut(300);

        $.ajax({
            url: "/group/member/delete",
            data: {accId: $(this).attr('id'), groupId: currentPageData.group.id},
            method: "GET"
        });
    });

    $("#results-table").on("click", ".btn[name='accept-member']", function () {
        $(this).parent().fadeOut(300);

        $.ajax({
            url: "/group/member/roleUpdate",
            data: {accId: $(this).attr('id'), groupId: currentPageData.group.id, roleId: 2},
            method: "PUT"
        });
    });

    $('#group-leave').on("click", function () {
        $(this).removeClass('list-group-item list-group-item-danger');
        $(this).addClass('list-group-item disabled');
        $(this).html('<span class="text-uppercase">group left</span>');

        $.ajax({
            url: "/group/leave/" + currentPageData.group.id,
            method: "DELETE"
        });
    });

    $('#group-send').on("click", function () {
        $(this).removeClass('list-group-item list-group-item-success');
        $(this).addClass('list-group-item disabled');
        $(this).html('<span class="text-uppercase">request sent</span>');

        $.ajax({
            url: "/group/member/send/" + currentPageData.group.id
        });
    });

    //friends
    $('#friend-remove').on("click", function () {
        $(this).removeClass('list-group-item list-group-item-danger');
        $(this).addClass('list-group-item disabled');
        $(this).html('<span class="text-uppercase">friend removed</span>');

        $.ajax({
            url: "/removeFriend",
            data: {
                id: currentPageData.acc.id
            }
        });
    });

    $('#friend-send').on("click", function () {
        $(this).removeClass('list-group-item list-group-item-success');
        $(this).addClass('list-group-item disabled');
        $(this).html('<span class="text-uppercase">request sent</span>');

        $.ajax({
            url: "/account/friends/requests/send/" + currentPageData.acc.id
        });
    });

    $("#friends").on("click", function (e) {
        e.preventDefault();
        var table = $("#results-table");
        var source = "/account/friends/" + currentPageData.acc.id;
        var target = "/account/";
        populateTableDisablePagination(table, source, target);
        showPage("#results-page");
    });

    $("#friendship-requests").on("click", function (e) {
        e.preventDefault();
        var table = $("#results-table");
        var source = "/account/friends/requests";
        var target = "/account/";
        populateTableDisablePagination(table, source, target);
        showPage("#results-page");
    });

    $("#results-table").on("click", ".btn[name='decline-friend']", function () {
        $(this).parent().fadeOut(300);

        $.ajax({
            url: "/account/friends/remove/" + $(this).attr('id')
        });
    });

    $("#results-table").on("click", ".btn[name='accept-friend']", function () {
        $(this).parent().fadeOut(300);

        $.ajax({
            url: "/account/friends/accept/" + $(this).attr('id')
        });
    });

    // phones
    $("#homephones-add").on("click", ".btn-success", function (e) {
        e.preventDefault();
        addPhone("#homephone");
    });

    $("#workphones-add").on("click", ".btn-success", function (e) {
        e.preventDefault();
        addPhone("#workphone");
    });

    $("#homephones").on("click", ".btn-danger", function (e) {
        e.preventDefault();
        $(this).parents(".input-group").remove();
    });

    $("#workphones").on("click", ".btn-danger", function (e) {
        e.preventDefault();
        $(this).parents(".input-group").remove();
    });

    function addPhone(inputPhone) {
        var columnTodAppend;
        var phoneIcon;
        var type;
        var index = -1;

        if (inputPhone.indexOf("home") >= 0) {
            columnTodAppend = $("#homephones");
            phoneIcon = "glyphicon glyphicon-phone";
            type = 0;
        } else {
            columnTodAppend = $("#workphones");
            phoneIcon = "glyphicon glyphicon-phone-alt";
            type = 1;
        }

        $('.index').each(function () {
            var value = parseInt($(this).val());
            index = Math.max(index, value) + 1;
        });

        var phone = $(inputPhone);
        var input = phone.val();
        var regex = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;

        if (regex.test(input)) {
            var markup =
                "<div class='input-group'>" +
                "<span class='input-group-addon'>" +
                "<i class='" + phoneIcon + "'></i>" +
                "</span>" +
                "<input type='text' name='phones[" + index + "].number' value='" + input + "' class='form-control' aria-describedby='sizing-addon2' readonly />" +
                "<input type='hidden' name='phones[" + index + "].type' value='" + type + "' />" +
                "<div class='input-group-btn'>" +
                "<button class='btn btn-danger btn-block' type='button' data-toggle='tooltip' title='Delete phone'>" +
                "<i class='glyphicon glyphicon-minus'></i>" +
                "</button>" +
                "</div>" +
                "<input class='index' type='hidden' value='" + index + "' />" +
                "</div>";

            columnTodAppend.append(markup);
            phone.val('');
        } else {
            $('#error-phone-modal').modal('show');
        }
    }

    function showPhones(phones) {
        $("#homephones").empty();
        $("#workphones").empty();

        $.each(phones, function (i) {
            var markup =
                "<div class='input-group'>" +
                "<span class='input-group-addon'>" +
                "<i class='glyphicon glyphicon-phone'></i>" +
                "</span>" +
                "<input type='text' name='phones[" + i + "].number' value='" + phones[i].number + "' class='form-control' aria-describedby='sizing-addon2' readonly />" +
                "<div class='input-group-btn'>" +
                "<button class='btn btn-danger btn-block' type='button' data-toggle='tooltip' title='Delete phone'>" +
                "<i class='glyphicon glyphicon-minus'></i>" +
                "</button>" +
                "</div>" +
                "<input class='index' type='hidden' value='" + i + "' />" +
                "<input type='hidden' name='phones[" + i + "].type'";

            if (phones[i].type === 0) {
                markup += " value='0'/></div>";
                $("#homephones").append(markup);
            } else {
                markup += " value='1'/></div>";
                $("#workphones").append(markup);
            }
        })
    }

    //datatables
    function populateTable(table, source, url) {
        $(table).dataTable({
            "fnDrawCallback": function (oSettings) {
                $(oSettings.nTHead).hide();
            },
            "bInfo": false,
            "bServerSide": true,
            "sAjaxSource": source,
            "bProcessing": false,
            "sPaginationType": "full_numbers",
            "bJQueryUI": true,
            "searching": false,
            "bLengthChange": false,
            "pageLength": 10,
            "bDestroy": true,
            "language": {
                "infoFiltered": ""
            },
            "aoColumnDefs": [
                {
                    "aTargets": [0], "visible": false
                },
                {
                    "aTargets": [1], "data": "Image2", "sClass": "center", "mRender": function (data, type, row) {
                    return "<a href='' onclick=showEntity('" + url + row[0] + "')><img src=data:image/jpg;base64," + row[3] + " class='img-thumbnail img-responsive' style='width:80px;'></a>"
                }
                },
                {
                    "aTargets": [2], "mRender": function (data, type, row, meta) {
                    return "<a href='' onclick=showEntity('" + url + row[0] + "')>" + row[1] + "</a>"
                }
                },
                {
                    "aTargets": [3], "mRender": function (data, type, row, meta) {
                    return "<a href='' onclick=showEntity('" + url + row[0] + "')>" + row[2] + "</a>"
                }
                }
            ]
        });
    }

    function populateTableDisablePagination(table, source, url) {
        var visible = source.indexOf("requests") >= 0 || source.indexOf("members") >= 0;

        $(table).dataTable({
            "fnDrawCallback": function (oSettings) {
                $(oSettings.nTHead).hide();
            },
            "bInfo": false,
            "bServerSide": true,
            "sAjaxSource": source,
            "bProcessing": false,
            "bPaginate": false,
            "bJQueryUI": true,
            "searching": false,
            "bLengthChange": false,
            "bDestroy": true,
            "language": {
                "infoFiltered": ""
            },
            "aoColumnDefs": [
                {
                    "aTargets": [0], "visible": false
                },
                {
                    "aTargets": [1], "data": "Image2", "sClass": "center", "mRender": function (data, type, row) {
                    return "<a href='' onclick=showEntity('" + url + row[0] + "')><img src=data:image/jpg;base64," + row[3] + " class='img-thumbnail img-responsive' style='width:80px;'></a>"
                }
                },
                {
                    "aTargets": [2], "mRender": function (data, type, row, meta) {
                    return "<a href='' onclick=showEntity('" + url + row[0] + "')>" + row[1] + ""
                }
                },
                {
                    "aTargets": [3], "mRender": function (data, type, row, meta) {
                    return "<a href='' onclick=showEntity('" + url + row[0] + "')>" + row[2] + ""
                }
                },
                {
                    "aTargets": [4], "visible": visible, "mRender": function (data, type, row, meta) {
                    if (source.indexOf("friends/requests") >= 0) {
                        return "<a class='btn btn-success btn-xs' name='accept-friend' id='" + row[0] + "' role='button'>accept</a>" +
                            "<a class='btn btn-danger btn-xs' name='decline-friend' id='" + row[0] + "' role='button'>decline</a>"
                    } else if (source.indexOf("member/requests") >= 0) {
                        return "<a class='btn btn-success btn-xs' name='accept-member' id='" + row[0] + "' role='button'>accept</a>" +
                            "<a class='btn btn-danger btn-xs' name='decline-member' id='" + row[0] + "' role='button'>decline</a>"
                    } else if (source.indexOf("members") >= 0 && groupRole() === 'AUTHOR') {
                        return "<a class='btn btn-danger btn-xs' name='decline-member' id='" + row[0] + "' role='button'>delete</a>"
                    } else {
                        return null;
                    }
                }
                }
            ]
        });
    }

    //status
    function showStatus(type, message) {
        var status = $("#status");
        status.removeClass().addClass("text-center alert alert-" + type);
        status.html(message);
    }

    $(document).click(function () {
        clearStatus();
    });

    function clearStatus() {
        var status = $("#status");
        status.removeClass();
        status.text("");
    }

    //group
    $("#group-registration").on("click", function (e) {
        e.preventDefault();
        showPage("#group-registration-page");
    });

    $("#group-form").submit(function (e) {
        e.preventDefault()
        var postData = $(this).serializeArray();

        $.ajax({
            url: "group/registration",
            method: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR) {
                showStatus("success", "group created");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showStatus("danger", "something wrong");
            }
        });
    });

    //search
    $("#search-field").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/account/search/autocomplete",
                data: {
                    entity: request.term
                },
                success: function (data) {
                    response($.map(data, function (account) {
                        return {value: account.id, label: account.firstName + " " + account.lastName}
                    }));
                }
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            showAcc("/account/" + ui.item.value);
        },
        focus: function (event, ui) {
            event.preventDefault();
            $("#search-field").val(ui.item.label);
        },
        search: function () {
            $(this).addClass('loading');
        },
        open: function () {
            $(this).removeClass('loading');
        }
    });

    $("#search-form").submit(function (e) {
        e.preventDefault();
        var accTable = $("#accounts-table");
        var accSource = "/account/search?entity=" + $('#search-field').val();
        var accUrl = "/account/";
        populateTable(accTable, accSource, accUrl);

        var groupTable = $("#groups-table");
        var groupSource = "/group/search?entity=" + $('#search-field').val();
        var groupUrl = "/group/";
        populateTable(groupTable, groupSource, groupUrl);

        showPage("#search-results-page");
    });

    //datepicker
    $(function () {
        $('#date').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    });

    //count friend requests for badge
    // $.ajax({
    //     url: "/account/friends/requests/count",
    //     success: function (data) {
    //         if (data > 0) {
    //             $('#count').text(data)
    //         } else {
    //             $('#count').text('')
    //         }
    //     }
    // });
});

var currentPageData = {
    acc: {},
    group: {}
};

//display data
function showEntity(url) {
    if (url.indexOf("account") >= 0) {
        showAcc(url)
    } else {
        showGroup(url)
    }
}

function groupRole() {
    var role;
    $.ajax({
        url: '/group/checkRole/' + currentPageData.group.id,
        method: 'GET',
        async: false,
        success: function (data) {
            if (data) {
                role = data.name;
            }
        }
    });
    return role;
}

function showAcc(url) {
    event.preventDefault();
    $.ajax({
            url: url,
            type: "GET",
            success: function (response) {
                currentPageData.acc = response;
                $("#account-avatar").attr("src", "data:image/jpg;base64," + response.image.base64);
                $("#name").text(response.firstName + ' ' + response.middleName + ' ' + response.lastName);
                if (response.birthday !== null) {
                    $("#birthday").text(response.birthday.dayOfMonth + ' ' + response.birthday.month + ' ' + response.birthday.year);
                }
                $("#address").text(response.address);
                $("#hobby-info").text(response.hobby);
                $("#skype-info").text(response.skype);
                $("#icq-info").text(response.icq);

                var phones = response.phones;

                $("#home-phones-info").empty();
                $("#work-phones-info").empty();

                $.each(phones, function (i) {
                    if (phones[i].type === 0) {
                        $("#home-phones-info").append("<p><i class='glyphicon glyphicon-phone'></i> " + phones[i].number + "</p>");
                    } else {
                        $("#work-phones-info").append("<p><i class='glyphicon glyphicon-phone-alt'></i> " + phones[i].number + "</p>");
                    }
                });

                $.ajax({
                    url: "/account/friends/isFriends/" + response.id,
                    type: "GET",
                    success: function (response) {
                        $("#friend-remove").addClass("hidden");
                        $("#friend-send").addClass('hidden');

                        if (response !== "SELF_ACCOUNT") {
                            if (response === "NOT_FRIENDS") {
                                $("#friend-send").removeClass('list-group-item disabled');
                                $("#friend-send").html('<span class="text-uppercase">send friend request</span>');
                                $("#friend-send").addClass('list-group-item list-group-item-success');
                                $("#friend-send").removeClass("hidden")
                            } else {
                                $("#friend-remove").removeClass('list-group-item disabled');
                                $("#friend-remove").html('<span class="text-uppercase">remove friend</span>');
                                $("#friend-remove").addClass('list-group-item list-group-item-danger');
                                $("#friend-remove").removeClass("hidden")
                            }
                        }
                    }
                });

                showPage("#account-page");
            }
        }
    )
}

function showGroup(url) {
    event.preventDefault();
    $.ajax({
        url: url,
        type: "GET",
        success: function (response) {
            currentPageData.group = response;
            $("#group-avatar").attr("src", "data:image/jpg;base64," + response.image.base64);
            $("#group-name").text(response.name);
            $("#group-direction").text(response.direction);

            $.ajax({
                url: "/group/checkRole/" + response.id,
                type: "GET",
                success: function (data) {
                    $("#group-send").addClass('hidden');
                    $("#group-leave").addClass("hidden");
                    $("#member-requests").addClass("hidden");

                    if (!data) {
                        $("#group-send").removeClass('list-group-item disabled');
                        $("#group-send").html('<span class="text-uppercase">send member request</span>');
                        $("#group-send").addClass('list-group-item list-group-item-success');
                        $("#group-send").removeClass("hidden")
                    } else {
                        if (data.name === 'AUTHOR' || data.name === 'MODERATOR') {
                            $("#member-requests").removeClass("hidden");
                        }

                        $("#group-leave").removeClass('list-group-item disabled');
                        $("#group-leave").html('<span class="text-uppercase">leave group</span>');
                        $("#group-leave").addClass('list-group-item list-group-item-danger');
                        $("#group-leave").removeClass("hidden")
                    }
                }
            });

            showPage("#group-page");
        }
    })
}

function showPage(page) {
    $("#account-page").addClass("hidden");
    $("#group-page").addClass("hidden");
    $("#login-form").addClass("hidden");
    $("#friends-page").addClass("hidden");
    $("#results-page").addClass("hidden");
    $("#members-page").addClass("hidden");
    $("#account-update-page").addClass("hidden");
    $("#account-update-xml-page").addClass("hidden");
    $("#account-update-avatar-page").addClass("hidden");
    $("#account-update-password-page").addClass("hidden");
    $("#friendship-requests-page").addClass("hidden");
    $("#search-results-page").addClass("hidden");
    $("#group-registration-page").addClass("hidden");
    $(page).removeClass("hidden");
}